function enviar(){
    var nombre=$("#nombre").html();
    var email=$("#email").html();
    var tel=$("#tel").html();
    var dat=$("#box").html();
    console.log(nombre + " " + email + " " + tel + " " + dat);
    $.ajax({
        url : './../../../tratarPeticiones.php',
        data : {"nombre" : nombre ,"email": email, "tel" : tel , "dat" : dat},
        type : 'POST',
        // el tipo de información que se espera de respuesta
        success : function(data) {
            console.log(data);
            if(data){
                $('.col-md-9').prepend("<div class='alert alert-success alert-dismissible fade show' role='alert'>La información se ha enviado. En breve nos pondremos en contacto contigo.<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>");
            }
            else{
                $('.col-md-9').prepend("<div class='alert alert-danger alert-dismissible fade show' role='alert'>Ha habido algún error, prueba de nuevo más tarde.<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>");
            }
        },
        error : function(jqXHR, status, error){
            $('.col-md-9').prepend("<div class='alert alert-warning alert-dismissible fade show' role='alert'>Ha habido un fallo de comunicación con el servidor. Prueba de nuevo más tarde.<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>");
        }
    });
}