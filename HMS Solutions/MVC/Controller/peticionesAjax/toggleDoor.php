<?php

if(file_exists('./MVC/Controller/YouAreLogged.php')) {
    require_once('./MVC/Controller/YouAreLogged.php');
}
else {
    header('Location: ../YouAreLogged.php');
}

if(isset($_POST["operacionPuerta"])){
    $resultadoPeticion = false;
    $accion = filter_var($_POST['operacionPuerta'], FILTER_SANITIZE_NUMBER_INT);
    if(isset($_POST['openCeldaNum'])) {
        $celda = filter_var($_POST['openCeldaNum'], FILTER_SANITIZE_STRING);

        //$celda = '010301';
        //$accion = 0;

        $habitacion = Habitacion::getByNumHabitacion($celda);
        $resultadoPeticion = toggle($habitacion, $accion, 1);
        
    }
    elseif(isset($_POST['openZonaId'])) {
        $zona = filter_var($_POST['openZonaId'], FILTER_SANITIZE_NUMBER_INT);
        //$zona = 3;
        //$accion = 1;  

        $habitaciones = Habitacion::getAllFromZona($zona);
        $habitacionesAnteriores = [];
        $accionesAnteriores = [];
        foreach($habitaciones as $habitacion) {

            array_push($habitacionesAnteriores, $habitacion);
            array_push($accionesAnteriores, ($accion % 2));

            $resultadoPeticion = toggle($habitacion, $accion, 2);

            if($resultadoPeticion == false) { // si se ha realizado la accion, generamos el evento, para que quede registrado.
                for($i = 0; $i < sizeof($habitacionesAnteriores); $i++) {
                    toggle($habitacionesAnteriores[$i], $accionesAnteriores[$i]);
                }
                break;
            }
        }
    }
    $res = ['res' => $resultadoPeticion];
    echo json_encode($res);  
} 

function toggle($habitacion, $accion, $m) {
    if(!is_null($habitacion) && is_object($habitacion)) {
        switch($accion) {
            case 0:
                if($habitacion->getEstadoPuerta() == 'CERRADA' || $m == 2) {
                    $habitacion->setEstadoPuerta('ABIERTA');
                    $resultado = $habitacion->guardar();
                    if($resultado == TRUE) {
                        $resultado = Habitacion::toggleDoor($habitacion->getNumHabitacion() . strval($accion));
                        if(!$resultado) {
                            $habitacion->setEstadoPuerta('CERRADA');
                            $habitacion->guardar();
                        }
                        else { //genera el log y cambia el estado de la zona
                            generarLog($habitacion, $accion);
                            $zona = $habitacion->getZona();
                            $habitaciones = Zona::getHabitaciones($zona->getId());
                            if(changeOpenZonaState($habitaciones)) { //una habitacion cerrada WARNING
                                $zona->setEstado('WARNING');
                            }
                            else { //ATENTION   
                                $zona->setEstado('ATENTION');
                            }
                            $zona->guardar();
                            return true;
                        }
                    }
                }
            break;
            case 1:
                if($habitacion->getEstadoPuerta() == 'ABIERTA' || $m == 2) {
                    $habitacion->setEstadoPuerta('CERRADA');
                    $resultado = $habitacion->guardar();
                    if($resultado == TRUE) {
                        $resultado = Habitacion::toggleDoor($habitacion->getNumHabitacion() . strval($accion));

                        if($resultado == false) {
                            $habitacion->setEstadoPuerta('ABIERTA');
                            $habitacion->guardar();
                        }
                        else { //genera el log y cambia el estado de la zona
                            generarLog($habitacion, $accion);
                            $zona = $habitacion->getZona();
                            $habitaciones = Zona::getHabitaciones($zona->getId());
                            if(changeCloseZonaState($habitaciones)) { //una habitacion cerrada WARNING
                                $zona->setEstado('WARNING');
                            }
                            else { //OK   
                                $zona->setEstado('OK');
                            }
                            $zona->guardar();
                            return true;
                        }
                    }
                }
            break;
        }     
    }
    return false;
}

function generarLog($habitacion, $accion) {
    $motivo = getMotivo();
    $usuario = Usuario::get(json_decode($_SESSION['login'])->{'id'});

    if(!is_null($motivo) && !is_null($usuario)) {
        $log = new LogPuerta();
        $log->setPosicion($habitacion->getEstadoPuerta());
        $log->setMotivoAccion($motivo);
        $log->setHabitacion($habitacion);
        $log->setUsuario($usuario);
        $log->guardar();
        return true;
    }
    return false;
}

function getMotivo() {
    $motivos = MotivoAccionLogPuerta::getAll();

    $motivo = NULL;
    foreach($motivos as $motivoo) {
        if($motivoo->getNombreMotivo() == 'Operación realizada por el usuario') {
            $motivo = $motivoo;
            break;
        }
    }

    if(is_null($motivo)) {
        $motivo = new MotivoAccionLogPuerta();
        $motivo->setNombreMotivo('Operación realizada por el usuario');
        $motivo->setDescripcion("Acción solicitada por un usuario validado.");
        if(is_null($motivo->guardar())) $motivo = null;
    }
    return $motivo;
}

function changeOpenZonaState($habitaciones) {
    if(!is_null($habitaciones) && is_array($habitaciones)) {
        $habitacionS = false;
        foreach($habitaciones as $habitacion) {
            if($habitacion->getEstadoPuerta() == 'CERRADA') {
                $habitacionS = true;
                break;
            }
        }

        return $habitacionS;
    }
}

function changeCloseZonaState($habitaciones) {
    if(!is_null($habitaciones) && is_array($habitaciones)) {
        $habitacionS = false;
        foreach($habitaciones as $habitacion) {
            if($habitacion->getEstadoPuerta() == 'ABIERTA') {
                $habitacionS = true;
                break;
            }
        }

        return $habitacionS;
    }
}

?>