<?php

if(file_exists('./MVC/Controller/YouAreLogged.php')) {
    require_once('./MVC/Controller/YouAreLogged.php');
}
else {
    header('Location: ../YouAreLogged.php');
}


if(isset($_POST['logs'])) {
    require_once('tools.php');
    $numLogs = filter_var($_POST['logs'], FILTER_SANITIZE_NUMBER_INT);
    //$numLogs = 20;

    $asignaciones = Asignacion::getAll();
    $movimientos = Movimiento::getAll();
    $logPuertas = LogPuerta::getAll();

    $todo = [];
    if(isset($_POST['zona-Id'])) {
        $zona = Zona::get(filter_var($_POST['zona-Id'], FILTER_SANITIZE_NUMBER_INT));
        //echo 'ok';
       // $zona = Zona::get(3);

        //print_r($zona->getData());

        if(!is_null($zona)) {
            foreach($asignaciones as $asignacion) {
                if($asignacion->getHabitacion()->getZona()->getId() == $zona->getId()) {
                    array_push($todo, $asignacion);
                    //echo 'getsAsignaciones<br />';
                }
            }

            foreach($movimientos as $movimiento) {
                if($movimiento->getHabitacion()->getZona()->getId() == $zona->getId()) {
                    array_push($todo, $movimiento);
                    //echo 'getsMovimientos<br />';
                }
            }

            foreach($logPuertas as $log) {
                if($log->getHabitacion()->getZona()->getId() == $zona->getId()) {
                    array_push($todo, $log);
                    //echo 'getsLogs<br />';
                }
            }
        }
        else {
            echo false;
            exit();
        }
    }
    else {
        foreach($asignaciones as $asignacion) {
            array_push($todo, $asignacion);
        }
    
        foreach($movimientos as $movimiento) {
            array_push($todo, $movimiento);
        }
    
        foreach($logPuertas as $log) {
            array_push($todo, $log);
        }
    }

    for($i = 0; $i < count($todo) - 1; $i++) {
        for($p = $i + 1; $p < count($todo); $p++){
            if(fechaComparator(getFecha($todo[$i]), getFecha($todo[$p])) != 0) {
                $obj = $todo[$i];
                $todo[$i] = $todo[$p];
                $todo[$p] = $obj;
            }
        }
    }

    $arr = [];
    foreach($todo as $t) {
        $log = new Log();
        $log->addData($t);
        array_push($arr, $log->getData());
    }

    print_r(json_encode($arr));

}
?>