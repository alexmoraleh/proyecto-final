<?php

    class Modulo implements ICurl {
        public static $add = 'modulo/add';
        public static $getAll = 'modulo/getAll';
        public static $get = 'modulo/get?id=';
        public static $delete = 'modulo/delete?id=';
        public static $getPrivilegio = 'modulo/getPrivilegio?id=';

        private $id;
        private $nombre;
        private $descripcion;
        private $logo;
        private $fechaAlta;
        private $fechaBaja;

        function __construct() {
            $this->fechaAlta = str_replace(' ', 'T', date('Y-m-d H:m:s.000O')); //añadimos la fecha de alta actual
        }
    
        public function getId() {
            return $this->id;
        }

        public function getNombre() {
            return $this->nombre;
        }

        public function setNombre($nombre) {
            $this->nombre = $nombre;
        }

        public function getDescripcion() {
            return $this->descripcion;
        }

        public function setDescripcion($descripcion) {
            $this->descripcion = $descripcion;
        }

        public function getLogo() {
            return $this->logo;
        }

        public function setLogo($logo) {
            $this->logo = $logo;
        }

        public function getFechaAlta() {
            return $this->fechaAlta;
        }

        public function getFechaBaja() {
            return $this->fechaBaja;
        }

        public function setFechaBaja($fecha) {
            $this->fechaBaja = $fecha;
        }

        public function getPrivilegio() {
            return $this->privilegio;
        }

        public function setPrivilegio($privilegio) {
            $this->privilegio = $privilegio;
        }

        public function toString() : String {
            $cadena = 'Modulo: id: ' . $this->getId() . ' nombre: ' . $this->getNombre() . ' descripcion: ' . $this->getDescripcion();
            return $cadena;
        }

        public function guardar() {
            $curl = CurlControlador::Singleton();
            $resultado = $curl->recogerDatosPOST(CurlControlador::$server . Modulo::$add, $this->getData());

            if(is_null($resultado)) {
                return NULL;
            }
            else{
                if(is_numeric($resultado)) {
                    if($resultado > 0) {
                        $this->id = $resultado;
                    }
                    return $resultado;
                }
                else {
                    return NULL;
                }
            }
        }

        //convierte el objeto a una array con el contenido de las variables a json
        public function getData() : Array
        {
            $var = get_object_vars($this);
            foreach($var as $value => &$val) {
                if (is_object($val) && method_exists($val,'getData')) {
                    $val = $val->getData();
                }
                elseif(is_array($val)) {
                    $array = [];
                    foreach($val as $localDate) {
                        if(is_object($localDate) && method_exists($localDate, 'getData')) {
                            array_push($array, $localDate->getData());
                        }
                        else {
                            array_push($array, $localDate);
                        }
                    }
                    $val = $array;
                }
            }
            return $var;
        }

        //añade los datos a este objeto
        public function addData($data)
        {
            foreach($data as $key => $value) {
                switch($key) {
                    default:
                        $this->{$key} = $value;
                }
            }
        }
    }

?>