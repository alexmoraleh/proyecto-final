<?php

    if(file_exists('./MVC/Controller/YouAreLogged.php')) {
        require_once('./MVC/Controller/YouAreLogged.php');
    }
    else {
        header('Location: ../YouAreLogged.php');
    }

    if(isset($_POST['perfilg'])) {
        // echo $_SESSION['login'];
        // Si al final implementamos lo de modificar el perfil, o actualizamos la sesion
        // o modificamos esto para que lo saque directamente de la BD
        $o->nombrec=json_decode($_SESSION['login'])->{'apellido'} . " " . json_decode($_SESSION['login'])->{'segundoApellido'} . ", " . json_decode($_SESSION['login'])->{'nombre'};
        $o->nombre=json_decode($_SESSION['login'])->{'nombre'};
        $o->mail=json_decode($_SESSION['login'])->{'mail'};

        $fecha = json_decode($_SESSION['login'])->{'fechaNacimiento'};
        $o->nacimiento=date_format(date_create($fecha), 'd/m/Y');
        
        $o->usuario=json_decode($_SESSION['login'])->{'nombreUsuario'};
        $o->alta=json_decode($_SESSION['login'])->{'fechaAlta'};
        $o->rol=json_decode($_SESSION['login'])->{'privilegiosUsuario'};
        $o->img=json_decode($_SESSION['login'])->{'imagenUsuario'};
        
        $myJSON = json_encode($o);
        echo $myJSON;
    }
?>