<?php

    class Calendario Implements ICurl {

        public static $add = 'calendario/add';
        public static $get = 'calendario/get?id='; //GET
        public static $getAll = 'calendario/getAll'; //GET
        public static $delete = 'calendario/delete?id='; //GET
        public static $getReglasDeAcciones = 'calendario/getReglasDeAcciones?id='; //GET

        public static function getCalendarioByDiaMesAnoHoraMinutoSegundo($dia, $mes, $ano, $hora, $minuto, $segundo) {
            $curl = CurlControlador::Singleton();
            $resultado = $curl->recogerDatosGET(CurlControlador::$server . 'calendario/getByDate?dia=' . intval($dia) . '&mes=' . intval($mes) . '&ano=' . intval($ano) . '&hora=' . intval($hora) . '&minuto=' . intval($minuto) . '&segundo=' . intval($segundo));
            //echo '<br />' . CurlControlador::$server . 'calendario/getByDate?dia=' . intval($dia) . '&mes=' . intval($mes) . '&ano=' . intval($ano) . '&hora=' . intval($hora) . '&minuto=' . intval($minuto) . '&segundo=' . intval($segundo) . '<br />';
            //print_r($resultado);

            if(is_null($resultado)) {
                return NULL;
            }
            else {
                $calendario = new Calendario();
                $calendario->addData($resultado);
                return $calendario;
            }
        }

        private $id;
        private $dia;
        private $mes;
        private $ano;
        private $hora;
        private $minuto;
        private $segundo;
        private $fechaAlta;
        private $fechaBaja;

        function __construct() {
            $this->fechaAlta = str_replace(' ', 'T', date('Y-m-d H:m:s.000O')); //añadimos la fecha de alta actual
        }

        public function getId() {
            return $this->id;
        }

        public function getDia() {
            return $this->dia;
        }

        public function setDia($dia) {
            $this->dia = $dia;
        }

        public function getMes() {
            return $this->mes;
        }

        public function setMes($mes) {
            $this->mes = $mes;
        }

        public function getAno() {
            return $this->ano;
        }

        public function setAno($ano) {
            $this->ano = $ano;
        }

        public function getHora() {
            return $this->hora;
        }

        public function setHora($hora) {
            $this->hora = $hora;
        }

        public function getMinuto() {
            return $this->minuto;
        }

        public function setMinuto($minuto) {
            $this->minuto = $minuto;
        }

        public function getSegundo() {
            return $this->segundo;
        }

        public function setSegundo($segundo) {
            $this->segundo = $segundo;
        }

        public function getFechaAlta() {
            return $this->fechaAlta;
        }

        public function getFechaBaja() {
            return $this->frchaBaja;
        }

        public function setFechaBaja($fecha) {
            $this->fechaBaja = $fecha;
        }

        public function toString() : String {
            $cadena = 'Calendario: id: ' . $this->getId();
            return $cadena;
        }

        public function guardar() {
            $curl = CurlControlador::Singleton();
            $resultado = $curl->recogerDatosPOST(CurlControlador::$server . Calendario::$add, $this->getData());

            if(is_null($resultado)) {
                return NULL;
            }
            else{
                if(is_numeric($resultado)) {
                    if($resultado > 0) {
                        $this->id = $resultado;
                    }
                    return $resultado;
                }
                else {
                    return NULL;
                }
            }
        }

        public function getData() : Array
        {
            $var = get_object_vars($this);
            foreach($var as $value => &$val) {
                if (is_object($val) && method_exists($val,'getData')) {
                    $val = $val->getData();
                }
                elseif(is_array($val)) {
                    $array = [];
                    foreach($val as $localDate) {
                        if(is_object($localDate) && method_exists($localDate, 'getData')) {
                            array_push($array, $localDate->getData());
                        }
                        else {
                            array_push($array, $localDate);
                        }
                    }
                    $val = $array;
                }
            }
            return $var;
        }

        public function addData($data)
        {
            foreach($data as $key => $value) {
                switch($key) {
                    default:
                        $this->{$key} = $value;
                }
            }
        }
    }

?>