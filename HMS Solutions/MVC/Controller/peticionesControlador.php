<?php
    require_once('./MVC/Model/BasicRequirements.php');
    
    if(isset($_SESSION['login'])) {
       
        require_once('peticionesAjax/getLogs.php');

        //get zonas
        require_once('peticionesAjax/getZonas.php');
            
        //enviar eventos para el calendario    
        require_once('peticionesAjax/getEvento.php');

        require_once('peticionesAjax/crearEvento.php');

        require_once('peticionesAjax/deleteEvento.php');

        require_once('peticionesAjax/getUsuario.php');

        require_once('peticionesAjax/getCellsFromZona.php');

        require_once('peticionesAjax/toggleDoor.php');
    }
    else {
        //login
        if(isset($_POST['nombreUsuario']) && isset($_POST['password'])) {
            $curl = CurlControlador::Singleton();
            print($curl->login($_POST['nombreUsuario'], $_POST['password']));
        }
        require_once('peticionesAjax/guardarWork.php');

    }
?>