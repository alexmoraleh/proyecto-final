<div id="logo_mobile_page" class="circle">
    <img src="./../Views/img/faviconwhite.png" alt="icon">
</div>

<div id="drag_header">
    <div class="drag_sector selected_page" onclick="selectSubmenu(this)">
        <span>Zonas</span>
    </div>
    <div class="drag_sector" onclick="selectSubmenu(this)">
        <span>Registros</span>
    </div>

    <!--<span onclick="selectSubmenu(this)" id="selected_sector">Zonas</span>-->

</div>

<!--<button onclick="openSearch()" class="fas fa-search" ></button>-->

<!--<div id="search">
    <input type="text" placeholder="Introduce la zona o el numero de la celda">
</div>-->

<!-- Aqui van los sectores para poder seleccionarlos y acceder a sus celdas -->

    <div id="sectors" >
        <div id="close_sector" class="fas fa-times" onclick="closeSector()"></div>
    
        
        
        <div id="sector_constrcutor">
        </div>


        <div id="cell_constructor">
                <div id="header_cell_constructor">
                    <div>
                        <h2>Nº Celda:</h2>
                    </div>
                    
                    <div>
                        <i class="fas fa-lock"></i>
                    </div>
                    <div>
                        <h2>Zona:</h2>
                        <h2>Tipo de Celda</h2>
                    </div>
                    
                </div>

                <div id="body_cell_constructor">
                    <h2>Reclusos asignados</h2>
                    <div class="recluso_cell">
                            <span>Nombre Completo</span>
                            <span>Foto preso</span>
                            <span>dni</span>
                            <span>Edad</span>
                            <span>Cargos</span>
                            <span>Observaciones a tener en cuenta:</span>
                    </div>
                    <div class="recluso_cell">
                            <span>Nombre Completo</span>
                            <span>Foto preso</span>
                            <span>dni</span>
                            <span>Edad</span>
                            <span>Cargos</span>
                            <span>Observaciones a tener en cuenta:</span>
                    </div>
                </div>
            </div>
    </div >

    <div id="status">

        <div id='constructor_log'>
            <div id="datos_log">
                <span>FECHA:</span>
                <span>ESTADO:</span>
                <span>TIPO:</span>
                <span>MOTIVO:</span>
            </div>

            <div id="datos_usuario">
                <span>NOMBRE:</span>
                <span>EMAIL:</span>
                <span>DNI:</span>
            </div>

            <div id="datos_carcel">
                <span>NUMERO:</span>
                <span>RECLUSO:</span>
                <span>ZONA:</span>
            </div>

            <div id="datos_recluso">
                <span>NOMBRE:</span>
                <span>EDAD:</span>
                <span>FECHA NAC:</span>
                <span>DNI:</span>
            </div>
        </div>
    </div>
    <div id="nicely_transparent_but_not_touchy_screen">
        <div id="notify_screen_box">
            <div id="notify_screen_text"></div>
            <div id="button_zone_notify">
                <button onclick="closeNotify()" style="background-color:#28a745;">Confirmar</button>
                <button onclick="closeNotify()" style="background-color:#dc3545;">Cancelar</button>
            </div>
        </div>
    </div>
     
    <div id="fake_menu_extra_height">
    </div>

    <div id="search">
        <button onClick="executeSearch()" class="fas fa-search"></button>
        <input type="text" placeholder="Introduce la zona o el numero de la celda"> 
    </div>

    <script>
        $.ajax({
            type:"post",
            async:false,
            url:"../Controller/peticionesAjax/getZones.php",
            success: function(data){
                data = $.parseJSON(data);
                $newCells = "";
                $newLogs = "";
                $.each(data, function (i, item) {
                    let nombre = item.nombre.split(" ");
                    let nom = "";
                    nombre.forEach(element => {
                        nom = nom + "." + element.substring(0,1).toUpperCase();
                    });
                    nom = nom.substring(1,nom.lengt);

                    $newCells = $newCells + " <div id='a"+ item.id +"' class='sector_module' onclick='openSector(this)'><span  class='sector_font'>"+ nom +"</span><span style='font-size:40px;'>"+ item.nombre +"</span></div>";
                    $newLogs = $newLogs + "<div id='s"+ item.id+"' onclick='logSector(this)' ";
                    
                    if(item.estado == "WARNING"){
                        $newLogs = $newLogs + "class='warning enhance_status'";
                        $newLogs = $newLogs + ">";
                        $newLogs = $newLogs + "<div class='class_line'> <span class='log_status_text' >WARNING</span>";
                    } else if(item.estado == "OK"){
                        $newLogs = $newLogs + "class='correct enhance_status'";
                        $newLogs = $newLogs + ">";
                        $newLogs = $newLogs + "<div class='class_line'><span class='log_status_text'>OK</span>";
                    } else {
                        $newLogs = $newLogs + "class='danger enhance_status'";
                        $newLogs = $newLogs + ">";
                        $newLogs = $newLogs + "<div class='class_line'><span class='log_status_text'>ATENTION</span>";
                    }
                    $newLogs = $newLogs + "<span style='font-size:40px'>"+ item.nombre +"</span></div>";
                    $newLogs = $newLogs + "<div id='la"+ item.id +"' class='content_module'><div class='tbl-header'><table cellpadding='0' cellspacing='0' ><thead><tr><th>Nº CELDA</th><th>FECHA</th><th>ACCION</th><th>CREADOR</th></tr></thead></table></div><div class='tbl-content'><table id='la"+ item.id +"' class='table_log_content' cellpadding='0' cellspacing='0' ><tbody></tbody></table></div></div></div>";

                });
                
                $("#sectors").append($newCells);
                $("#status").append($newLogs);
            }
        });
    </script>
