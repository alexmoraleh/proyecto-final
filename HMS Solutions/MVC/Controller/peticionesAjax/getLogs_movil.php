<?php
    session_start();
    require_once('../../Model/BasicRequirements.php');
    require_once('tools.php');
    $numLogs = filter_var(1000, FILTER_SANITIZE_NUMBER_INT);
    //$numLogs = 20;

    $asignaciones = Asignacion::getAll();
    $movimientos = Movimiento::getAll();
    $logPuertas = LogPuerta::getAll();

    $todo = [];

    if(isset($_POST['zona-Id'])) {
        $zona = Zona::get(filter_var($_POST['zona-Id'], FILTER_SANITIZE_NUMBER_INT));
        //$zona = Zona::get(3);

        if(!is_null($zona)) {
            foreach($asignaciones as $asignacion) {
                if($asignacion->getHabitacion()->getZona()->getId() == $zona->getId()) {
                    array_push($todo, $asignacion);
                }
            }

            foreach($movimientos as $movimiento) {
                if($movimiento->getHabitacion()->getZona()->getId() == $zona->getId()) {
                    array_push($todo, $movimiento);
                }
            }

            foreach($logPuertas as $log) {
                if($log->getHabitacion()->getZona()->getId() == $zona->getId()) {
                    array_push($todo, $log);
                }
            }
        }
        else {
            echo false;
            exit();
        }
    }
    else {
        foreach($asignaciones as $asignacion) {
            array_push($todo, $asignacion);
        }
    
        foreach($movimientos as $movimiento) {
            array_push($todo, $movimiento);
        }
    
        foreach($logPuertas as $log) {
            array_push($todo, $log);
        }
    }

    for($i = 0; $i < count($todo) - 1; $i++) {
        for($p = $i + 1; $p < count($todo); $p++){
            if(fechaComparator(getFecha($todo[$i]), getFecha($todo[$p])) != 0) {
                $obj = $todo[$i];
                $todo[$i] = $todo[$p];
                $todo[$p] = $obj;
            }
        }
    }

    $arr = [];
    foreach($todo as $t) {
        $log = new Log();
        $log->addData($t);
        array_push($arr, $log->getData());
    }

    print_r(json_encode($arr));

?>