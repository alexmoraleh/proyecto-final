function eliminarNodes(padre) {
	$('#'+padre).children().each(function () {
		$(this).remove();
	});
}

/*añade un modulo*/
function crearModulo(id, edificio, planta, zona, descripcion, estado) {
	let newStatusBox = document.createElement('div');
	newStatusBox.className += 'status-box';
    newStatusBox.id = id;
    
    newStatusBox.addEventListener('click', function() { //añade un onclick el qual llama a ver log y le pasa su id
        getLogs(this.id); //ojo, cambiar
    });

	let presentacion = document.createElement('div');
	presentacion.className += 'status-presentacion-box';
	estilo = null;
	switch(estado) {
		case 'WARNING':
				presentacion.style='background-color: var(--keys)';
		break;
		case 'ATENTION':
			presentacion.style='background-color: var(--keyClose)';
		break;
		default:
			presentacion.style='background-color: var(--jeyOpen)';
	}

	presentacion.innerHTML = '<span class="iniciales">' + getIniciales(zona) + '</span>' +
	'<br /><span class="nombre">' + zona + '</span>' ;

	let statusbox = document.createElement('div');
	statusbox.className += 'status-info-box';
	statusbox.innerHTML = '<span><u>Edificio:</u> ' + edificio + ' <u>Planta:</u> ' + planta + '</span>'+
    '<span><u>Descripción:</u> ' + descripcion +'</span>';

	newStatusBox.appendChild(presentacion);
	newStatusBox.appendChild(statusbox);

	return newStatusBox;
}

function getIniciales(cadena) {
	separador = " ", // un espacio en blanco
	arregloDeSubCadenas = cadena.split(separador); // SEPARA EL NOMBRE EN CADENAS INDIVIDUALES
	resultado = '';
    // IMPRIME LA PRIMERA LETRA DE CADA CADENA 
    for (x=0;x<arregloDeSubCadenas.length;x++){
        subCadena = arregloDeSubCadenas[x].substring(0, 1);
        resultado += subCadena.toUpperCase() + ". ";
	}
	return resultado;
}

function apendar(elemento, idPadre) {
	document.getElementById(idPadre).appendChild(elemento);
}

function crearLog(elemento) {
	let padre = document.createElement('div');
	padre.className += 'log-box';

	let header = document.createElement('div');
	header.className += 'log-header';

	header.innerHTML = '<span><u>Fecha:</u> ' + elemento.fechaAccion.replace('T', ' ').replace('.000+0000', '') + '</span>';
	
	header.innerHTML += '<span><u>Celda:</u> ' + elemento.celda + '</span>';

	padre.appendChild(header);

	let masInformacion = document.createElement('div');
	masInformacion.className += 'log-body';

	if(elemento.creador == 'USUARIO') {
		masInformacion.innerHTML += '<span><u>Usuario:</u> ' + elemento.datosCreador + '</span>';
	}
	if(elemento.creador == 'RECLUSO') {
		masInformacion.innerHTML += '<span><u>Recluso:</u> ' + elemento.datosCreador + '</span>';
	}

	masInformacion.innerHTML += '<span><u>Acción:</u> ' + elemento.estado + '</span>';

	if(elemento.tipoLog == 'LOG') {
		masInformacion.innerHTML += '<span><u>Motivo:</u> ' + elemento.motivo + '</span>';
	}

	padre.appendChild(masInformacion);

	return padre;
}