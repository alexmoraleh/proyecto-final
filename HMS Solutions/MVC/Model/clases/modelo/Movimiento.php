<?php

    class Movimiento {
        public static $add = 'movimiento/add';
        public static $get = 'movimiento/get?id=';
        public static $getAll = 'movimiento/getAll';
        public static $delete = 'movimiento/delete?id=';
        public static $getRecluso = 'movimiento/getRecluso?id=';
        public static $getHabitacion = 'movimiento/getHabitacion?id=';

        public static function get($id) {
            $curl = CurlControlador::Singleton();
            $resultado = $curl->recogerDatosGET(CurlControlador::$server . Movimiento::$get . $id);

            if(is_null($resultado)) {
                return NULL;
            }
            else{
                $movimiento = new Movimiento();
                $movimiento->addData($resultado);
                return $movimiento;
            }
        }

        public static function getAll() {
            $curl = CurlControlador::Singleton();
            $resultado = $curl->recogerDatosGET(CurlControlador::$server . Movimiento::$getAll);

            if(is_null($resultado)) {
                return NULL;
            }
            else{
                $movimientos = [];

                foreach($resultado as $array) {
                    $movimiento = new Movimiento();
                    $movimiento->addData($array);
                    array_push($movimientos, $movimiento);
                }
               
                return $movimientos;
            }
        }

        private $id;
        private $fechaAccion;
        private $entrada;
        private $habitacion;
        private $recluso;

        function __construct() {
            $this->fechaAccion = str_replace(' ', 'T', date('Y-m-d H:m:s.000O')); //añadimos la fecha de alta actual
        }

        public function getId() {
            return $this->id;
        }

        public function getFechaAccion() {
            return $this->fechaAccion;
        }

        public function setFechaAccion($fecha) {
            $this->fechaAccion = $fecha;
        }

        public function getEntrada() {
            return $this->entrada;
        }

        public function setEntrada($entrada) {
            $this->entrada = $entrada;
        }

        public function getHabitacion() {
            return $this->habitacion;
        }

        public function setHabitacion($habitacion) {
            $this->habitacion = $habitacion;
        }

        public function getRecluso() {
            return $this->recluso;
        }

        public function setRecluso($recluso) {
            $this->recluso = $recluso;
        }

        public function toString() : String {
            $cadena = 'Movimiento: id: ' . $this->id . ' fechaAccion: ' . $this->fechaAccion . ' entrada: ' . $this->entrada;
            $cadena = $cadena . '<br />->' . $this->habitacion->toString();
            $cadena = $cadena . '<br />-> ' . $this->recluso->toString();
            return $cadena;
        }

        public function guardar() {
            $curl = CurlControlador::Singleton();
            $resultado = $curl->recogerDatosPOST(CurlControlador::$server . Movimiento::$add, $this->getData());

            if(is_null($resultado)) {
                return NULL;
            }
            else{
                if(is_numeric($resultado)) {
                    if($resultado > 0) {
                        $this->id = $resultado;
                    }
                    return $resultado;
                }
                else {
                    return NULL;
                }
            }
        }

        //convierte el objeto a una array con el contenido de las variables a json
        public function getData() : Array
        {
            $var = get_object_vars($this);
            foreach($var as $value => &$val) {
                if (is_object($val) && method_exists($val,'getData')) {
                    $val = $val->getData();
                }
                elseif(is_array($val)) {
                    $array = [];
                    foreach($val as $localDate) {
                        if(is_object($localDate) && method_exists($localDate, 'getData')) {
                            array_push($array, $localDate->getData());
                        }
                        else {
                            array_push($array, $localDate);
                        }
                    }
                    $val = $array;
                }
            }
            return $var;
        }

        //añade los datos a este objeto
        public function addData($data)
        {
            foreach($data as $key => $value) {
                switch($key) {
                    case 'habitacion':
                        $this->habitacion = new Habitacion();
                        $this->habitacion->addData($data->{$key});
                    break;
                    case 'recluso':
                        $this->recluso = new Recluso();
                        $this->recluso->addData($data->{$key});
                    break;
                    default:
                        $this->{$key} = $value;
                }
            }
        }
    }


?>