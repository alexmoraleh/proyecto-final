<?php

    class Log {
        
        private $celda; //NUMERO DE LA CELDA
        private $fechaAccion; //FECHA EN QUE SE REALIZO EL LOG
        private $tipoLog; //tipo de log, MOVIMIENTO, ASIGNACION, LOGPUERTA
        private $estado; //CERRADA, ABIERTA, ASIGNADO, DESASIGNADO, ENTRADA, SALIDA

        //usuario/preso/evento
        private $creador;
        private $datosCreador;
        private $idCreador;

        //el motivo de por que se ha realizado esta accion
        private $motivo;




        public function addData($datos) {
            if(!is_null($datos)) {
                $this->celda = $datos->getHabitacion()->getNumHabitacion();
                switch(get_class($datos)) {
                    case 'Asignacion':
                        
                        $this->tipoLog = 'ASIGNACION';

                        if(is_null($datos->getFechaEliminacion())) {
                            $this->estado = 'ASIGNADO';
                            $this->fechaAccion = $datos->getFechaRealizacion();
                        }
                        else{
                            $this->estado = 'DESASIGNADO';
                            $this->fechaAccion = $datos->getFechaEliminacion();
                        }

                        $recluso = $datos->getRecluso();
                        $this->creador = 'RECLUSO';
                        $this->datosCreador = $recluso->getApellido() . ' ' . $recluso->getSegundoApellido() . ', ' . $recluso->getNombre();
                        $this->idCreador = $recluso->getId();
                    break;
                    case 'Movimiento':
                        $this->fechaAccion = $datos->getFechaAccion();
                        if($datos->getEntrada()) {
                            $this->estado = 'ENTRADA';
                        }
                        else {
                            $this->estado = 'SALIDA';
                        }
                        $this->tipoLog = 'MOVIMIENTO';

                        $recluso = $datos->getRecluso();
                        $this->creador = 'RECLUSO';
                        $this->datosCreador = $recluso->getApellido() . ' ' . $recluso->getSegundoApellido() . ', ' . $recluso->getNombre();
                        $this->idCreador = $recluso->getId();
                    break;
                    case 'LogPuerta':
                        $this->fechaAccion = $datos->getFechaRealizacion();
                        $this->estado = $datos->getPosicion();

                        if(is_null($datos->getUsuario())) {
                            $this->tipoLog = 'EVENTO';

                            $this->creador = 'EVENTO';
                            $this->idCreador = $datos->getReglaDeAccion()->getId();

                            $this->motivo = $datos->getReglaDeAccion()->getDescripcion();
                        }
                        else {
                            $this->tipoLog = 'LOG';

                            $usuario = $datos->getUsuario();
                            $this->creador = 'USUARIO';
                            $this->datosCreador = $usuario->getApellido() . ' ' . $usuario->getSegundoApellido() . ', ' . $usuario->getNombre();
                            $this->idCreador = $usuario->getId();

                            $this->motivo = $datos->getMotivoAccion()->getDescripcion();;
                        }
                    break;
                }
            }
        }

        public function getData() {
            $var = get_object_vars($this);
            foreach($var as $value => &$val) {
                if (is_object($val) && method_exists($val,'getData')) {
                    $val = $val->getData();
                }
                elseif(is_array($val)) {
                    $array = [];
                    foreach($val as $localDate) {
                        if(is_object($localDate) && method_exists($localDate, 'getData')) {
                            array_push($array, $localDate->getData());
                        }
                        else {
                            array_push($array, $localDate);
                        }
                    }
                    $val = $array;
                }
            }
            return $var;
        }
    }

?>