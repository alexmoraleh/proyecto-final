<?php
session_start();
require_once('../../Model/BasicRequirements.php');


if(isset($_POST["zona"])){
    $zona = substr($_POST["zona"], 0, -1);
    
    $habitaciones = [];
    foreach(Habitacion::getAll() as $habitacion) {
        array_push($habitaciones, $habitacion);
    }
    
    $habitacionPreso = null;
    foreach($habitaciones as $hab){
        if(strval($hab->getZona()->getId()) == $zona){
            $bool = Habitacion::toggleDoor($hab->getNumHabitacion());

            $habitacionPreso = $hab;
            
        }
        $bool = false;
        $toggle = $_POST["zona"];
        $toggle = substr($toggle, (strlen($toggle) - 1), strlen($toggle));
        if($habitacionPreso != null){ 
            if($toggle == "1"){
                $habitacionPreso->setEstadoPuerta('ABIERTA');
            } else {
                $habitacionPreso->setEstadoPuerta('CERRADA');
            }
            $habitacionPreso->guardar();
            $habitacionPreso = null;
        }
    }
    
    print_r($bool);
} 