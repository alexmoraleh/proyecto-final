<?php

    if(file_exists('./MVC/Controller/YouAreLogged.php')) {
        require_once('./MVC/Controller/YouAreLogged.php');
    }
    else {
        header('Location: ../YouAreLogged.php');
    }

    if(isset($_POST['dia']) && isset($_POST['mes']) && isset($_POST['ano']) && isset($_POST['hora']) && isset($_POST['minuto']) && isset($_POST['segundo']) && isset($_POST['numHabitacion']) && isset($_POST['descripcion']) && isset($_POST['tipo'])) {
        $dia = intval(filter_var($_POST['dia'], FILTER_SANITIZE_STRING));
        $mes = intval(filter_var($_POST['mes'], FILTER_SANITIZE_STRING));
        $ano = intval(filter_var($_POST['ano'], FILTER_SANITIZE_STRING));
        $hora = intval(filter_var($_POST['hora'], FILTER_SANITIZE_STRING));
        $minuto = intval(filter_var($_POST['minuto'], FILTER_SANITIZE_STRING));
        $segundo = intval(filter_var($_POST['segundo'], FILTER_SANITIZE_STRING));
        $descripcion = filter_var($_POST['descripcion'], FILTER_SANITIZE_STRING);
        $accion = filter_var($_POST['tipo'], FILTER_SANITIZE_STRING);
        $habitaciones = $_POST['numHabitacion'];

        /*$dia = 1;
        $mes = 5;
        $ano = 2019;
        $hora = 12;
        $minuto = 5;
        $segundo = 00;
        $descripcion = 'nuevo';
        $accion = 'ABRIR';
        $habitaciones = ['020202', '020203'];*/


        $calendario = Calendario::getCalendarioByDiaMesAnoHoraMinutoSegundo($dia, $mes, $ano, $hora, $minuto, $segundo);

        if(is_null($calendario)) {
            $calendario = new Calendario();
            $calendario->setDia($dia);
            $calendario->setMes($mes);
            $calendario->setAno($ano);
            $calendario->setHora($hora);
            $calendario->setMinuto($minuto);
            $calendario->setSegundo($segundo);
            if(is_null($calendario->guardar())) {
                echo false;
                exit();
            }
        }

        $user = new Usuario();
        $user->addData(json_decode($_SESSION['login'], false));

        $reglaDeAccion = new ReglaDeAccion();
        $reglaDeAccion->setNombreRegla($user->getNombreUsuario() . $calendario->getId());
        $reglaDeAccion->setDescripcion($descripcion);
        $reglaDeAccion->setTipoAccion($accion);//ABRIR, CERRAR
        $reglaDeAccion->setUsuario($user);
        $reglaDeAccion->setCalendario($calendario);

        /*print_r($calendario->toString()); 
        echo '<br />';
        print_r($reglaDeAccion->getDescripcion());*/

        foreach($habitaciones as $num) {
            $num = filter_var($num, FILTER_SANITIZE_STRING);
            $habitacion = Habitacion::getByNumHabitacion($num);
            if(is_null($habitacion)) {
                echo false;
                exit();
            }
            else {
                $array = $reglaDeAccion->getHabitaciones();
                array_push($array, $habitacion);
                $reglaDeAccion->setHabitaciones($array);
            }
        }
        $reglaDeAccion->guardar();
        echo is_null($reglaDeAccion->getId())? false : true;
    }
?>