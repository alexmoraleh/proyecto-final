<?php

    class LogPuerta implements ICurl{
        public static $add = 'logPuerta/add';
        public static $get = 'logPuerta/get?id='; //GET
        public static $getAll = 'logPuerta/getAll'; //GET
        public static $delete = 'logPuerta/delete?id='; //GET
        public static $getUsuario = 'logPuerta/getUsuario?id='; //GET
        public static $getMotivoAccionLogPuerta = 'logPuerta/getMotivoAccionLogPuerta?id='; //GET
        public static $getHabitacion = 'logPuerta/getHabitacion?id='; //GET

        public static function get($id) {
            $curl = CurlControlador::Singleton();
            $resultado = $curl->recogerDatosGET(CurlControlador::$server . LogPuerta::$get . $id);

            if(is_null($resultado)) {
                return NULL;
            }
            else{
                $log = new LogPuerta();
                $log->addData($resultado);
                return $log;
            }
        }

        public static function getAll() {
            $curl = CurlControlador::Singleton();
            $resultado = $curl->recogerDatosGET(CurlControlador::$server . LogPuerta::$getAll);

            if(is_null($resultado)) {
                return NULL;
            }
            else{
                $logs = [];

                foreach($resultado as $array) {
                    $log = new LogPuerta();
                    $log->addData($array);
                    array_push($logs, $log);
                }
               
                return $logs;
            }
        }

        private $id;
        private $fechaRealizacion;
        private $posicion; //esto es un enum, ojo
        private $motivoAccion; //objeto
        private $habitacion; //objeto
        private $usuario; //objeto
        private $regla;

        function __construct() {
            $this->fechaRealizacion = str_replace(' ', 'T', date('Y-m-d H:m:s.000O')); //añadimos la fecha de alta actual
        }
        
        public function getId() {
            return $this->id;
        }

        public function getFechaRealizacion() {
            return $this->fechaRealizacion;
        }

        public function getPosicion() {
            return $this->posicion;
        }

        public function setPosicion($posicion) {
            $this->posicion = $posicion;
        }

        public function getMotivoAccion() {
            return $this->motivoAccion;
        }

        public function setMotivoAccion($motivo) {
            $this->motivoAccion = $motivo;
        }

        public function getHabitacion() {
            return $this->habitacion;
        }

        public function setHabitacion($habitacion) {
            $this->habitacion = $habitacion;
        }

        public function getUsuario() {
            return $this->usuario;
        }

        public function setUsuario($usuario) {
            $this->usuario = $usuario;
        }

        public function getRegla() {
            return $this->regla;
        }

        public function setRegla($regla) {
            $this->regla = $regla;
        }

        public function toString() : String {
            $cadena = 'LogPuerta: id: ' . $this->getId() . ' fechaRealizacion: ' . $this->getFechaRealizacion() . ' posicion: ' . $this->getPosicion();
            if(!is_null($this->usuario) && !is_null($this->motivoAccion)) {
                $cadena = $cadena . '<br />-> ' . $this->motivoAccion->toString();
                $cadena . '<br />-> ' . $this->usuario->toString();
            }
            elseif(!is_null($this->regla)) {
                $cadena = $cadena . '<br />' . $this->regla->toString();
            }
            return $cadena;
        }

        public function guardar() {
            $curl = CurlControlador::Singleton();
            $resultado = $curl->recogerDatosPOST(CurlControlador::$server . LogPuerta::$add, $this->getData());
            if(!is_null($resultado)) {
                if($resultado > 0) {
                    $this->id = $resultado;
                    return true;
                }
            }
            return false;
        }

        //convierte el objeto a una array con el contenido de las variables a json
        public function getData() : Array
        {
            $var = get_object_vars($this);
            foreach($var as $value => &$val) {
                if(!is_null($val)) {
                    if (is_object($val) && method_exists($val,'getData')) {
                        $val = $val->getData();
                    }
                    elseif(is_array($val)) {
                        $array = [];
                        foreach($val as $localDate) {
                            if(is_object($localDate) && method_exists($localDate, 'getData')) {
                                array_push($array, $localDate->getData());
                            }
                            else {
                                array_push($array, $localDate);
                            }
                        }
                        $val = $array;
                    }
                }
            }
            return $var;
        }

        //añade los datos a este objeto
        public function addData($data)
        {
            foreach($data as $key => $value) {
                if(!is_null($value)) {
                    switch($key) {
                        case 'motivoAccion':
                            $this->motivoAccion = new MotivoAccionLogPuerta();
                            $this->motivoAccion->addData($value);
                        break;
                        case 'habitacion':
                            $this->habitacion = new Habitacion();
                            $this->habitacion->addData($value);
                        break;
                        case 'usuario':
                            $this->usuario = new Usuario();
                            $this->usuario->addData($value);
                        break;
                        case 'regla':
                            $this->regla = new ReglaDeAccion();
                            $this->regla->addData($value);
                        break;
                        default:
                            $this->{$key} = $value;
                    }
                }
            }
        }
    }
?>