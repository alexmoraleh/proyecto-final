<script>
    if( screen.width <= 480 ) {
        window.location.href = "./MVC/Controller/controlador_movil.php";  
    }
</script>

<div id="login_body">
    <div class="login">
        <div class="heading">
            <h2>Sign in</h2>
            <form id="login_form" method="post" onsubmit="ajaxLogin()">

                <div class="input-group input-group-lg">
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    <input type="text" class="form-control" placeholder="Usuario" name="nombreUsuario">
                </div>

                <div class="input-group input-group-lg">
                    <span class="input-group-addon"><i class="fa fa-lock" id="tonto"></i></span>
                    <input type="password" id="pass_text" class="form-control" placeholder="Contraseña" name="password">
                </div>

                <button type="button" onclick="ajaxLogin()" class="float">Login</button>
                <div id="notify_bar"></div>
            </form>
        </div>
    </div>
</div>

<script>
function ajaxLogin(){
    console.log('ok');
    var e = $("#login_form");
    var urll = '';
    <?php
        if(file_exists('./index.php')) {
            echo 'urll = "./tratarPeticiones.php";';
        }
        else {
            echo 'urll = "./../../tratarPeticiones.php";';
        }
    ?>

    // this.preventDefault(); // avoid to execute the actual submit of the form.
    var form = $(e);
    $.ajax({
           type: "POST",
           url:  urll,
           data: form.serialize(), // serializes the form's elements.
           success: function(data)
           {
               if(data == false){
                    let notfy = "<span style='color:red; font-size:40px;'>Incorrect password or user.</span>";
                    $("#notify_bar").children().remove();
                    $("#notify_bar").append(notfy); // show response from the php script.
                    $("#pass_text").val('');
               } else {
                    <?php
                        if(file_exists('./index.php')) {
                            echo 'window.location.href = "./index.php";';
                        }
                        else {
                            echo 'window.location.href = "./../../index.php";';
                        }
                    ?>
               }
           },
           error: function(){
            //    alert("fallo");
                console.log('error: ');
           }
         });
}
// $("#login_form").submit(ajaxLogin(this));

</script>