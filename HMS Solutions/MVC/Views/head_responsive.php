<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="../Views/js/moment.js"></script>
        <script src="../Views/js/es.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" id="bootstrap-css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css" />
        
        <link rel="stylesheet" type="text/css" href="../Views/css/movil_responsive.css">
        <link rel="icon" href="../Views/img/faviconwhite.png"><!--Ruta absoluta, hojo-->
        <link rel="stylesheet" href="../Views/Common/pace.css" type="text/css"/>

        <link rel="stylesheet" type="text/css" href="../Views/css/movil_responsive.css">
        <link rel="stylesheet" href="../Views/css/navBar.css" type="text/css"/>
        <script src="../Views/js/responsive.js"></script>
        <script src="../Views/js/pace.min.js"></script>
        <?php
            echo $style;
        ?>
        
    </head>
<body>

<?php 
?>