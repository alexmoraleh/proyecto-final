#include <Servo.h>
 
Servo myservo;  // crea el objeto servo
const int led = 10; // pin del led
 
int pos = myservo.read();    // posicion del servo
int atc = 9;
String aux="";
String rd="";

void setup() {
   myservo.attach(9);  // vincula el servo al pin digital 9
   Serial.begin(9600);
   pinMode(led,OUTPUT);
}
 
void loop() {
   //varia la posicion de 0 a 180, con esperas de 15ms
   if(Serial.available()>0){
    rd=Serial.read();
    aux=rd.charAt(0);
    Serial.print(rd);
    atc=aux.toInt();
    myservo.attach(atc);
    String o = rd.substring(1,1);//substring(1);
    //Serial.print(atc + o);
    if(o=="0"){
      for (pos = pos; pos <= 180; pos += 1){
        digitalWrite(led,HIGH);
        myservo.write(pos);       
        delay(5);
        digitalWrite(led,LOW);              
      }
    }
    else{
      for (pos = pos; pos >= 0; pos -= 1){
        digitalWrite(led,HIGH);
        myservo.write(pos);    
        delay(5);
        digitalWrite(led,LOW);          
      }
    }     
   }
}