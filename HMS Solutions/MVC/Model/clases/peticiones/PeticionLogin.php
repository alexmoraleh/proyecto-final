<?php

    class PeticionLogin implements ICurl{
        private $nombreUsuario;
        private $mail;
        private $password;

        function _construct($nombre, $password) {
            $this->nombreUsuario = $nombre;
            $this->password = $password;
        }

        public function setNombreUsuario($nombre) {
            $this->nombreUsuario = $nombre;
        }

        public function setPassword($password) {
            $this->password = $password;
        }

        public function setMail($mail) {
            $this->mail = $mail;
        }

        public function toString() {
            return 'PeticionLogin: nombreUsuario: ' . $this->nombreUsuario . ' mail: ' . $mail . ' password: ' . $this->password;
        }

        public function getData() : Array
        {
            $var = get_object_vars($this);
            foreach($var as $value => &$val) {
                if (is_object($val) && method_exists($val,'getData')) {
                    $val = $val->getData();
                }
            }
            return $var;
        }

        public function addData($data)
        {
            foreach (get_object_vars($this) as $key => $value) {
                switch($key) {
                    default:
                        $this->{$key} = $data->{$key};
                }
            }
        }
    }

?>