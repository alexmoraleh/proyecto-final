<?php

class CurlControlador {
    public static $server = 'http://localhost:8080/';

    private $opciones;
    private $header;
    
    private static $conexion;

    public static function Singleton() : CurlControlador
    {
        if (isset($conexion) == false) {
            $conexion = new CurlControlador();
        }
        return $conexion;
    }

    private function __construct()
    {
        $this->opciones = array(
            CURLOPT_RETURNTRANSFER => true, //indica si devuelve valores, se le pueden añadir numeros ej 1
            CURLOPT_ENCODING => "utf-8", //encoding, UTF-8 porfavor
            CURLOPT_TIMEOUT => 30, //timeout
            /*CURLOPT_SSL_VERIFYPEER => true,
            CURLOPT_SSL_VERIFYHOST => 2, //intenta validar el host 2 veces   
            CURLOPT_CAINFO => getcwd().'./certificados/localhost.crt' //ruta al certificado raiz/...*/
        );

        $this->header = array(
            'Content-Type: application/json'
        );
    }

    //añade la cabecera de la peticion
    //modificar la sesion
    private function headerConfiguration($cliente) {
        if(isset($_SESSION['login'])){ 
            $localHeader = $this->header;
            
            array_push($localHeader, 'Autorizacion: ' . json_decode($_SESSION['login'])->{'token'}); 
            //la palabra Autorizacion es por un tema de seguridad del web service...
            curl_setopt($cliente, CURLOPT_HTTPHEADER, $localHeader);
        }
        else {
            curl_setopt($cliente, CURLOPT_HTTPHEADER, $this->header);
        }
    }

    private function getGETConfiguration($url) 
    {
        $cliente = curl_init($url);
        curl_setopt_array($cliente, $this->opciones); //cargamos el diccionario on las opciones
        $this->headerConfiguration($cliente);
        return $cliente;
    }

    private function getPOSTConfiguration($url) 
    {
        $cliente = $this->getGETConfiguration($url);
        curl_setopt($cliente, CURLOPT_POST, 1);
        curl_setopt($cliente, CURLOPT_RETURNTRANSFER, true);
        return $cliente;
    }

    public function recogerDatosGET($url) 
    {
        $cliente = $this->getGETConfiguration($url);

        //ejecucion
        $result=curl_exec($cliente); //ejecutamos curl
        $errno = curl_errno($cliente); //ultimo codigo de error
        $error = curl_error($cliente); //devuelve una cadena con el ultimo codigo de error
        curl_close($cliente); //cerramos

        if($errno != 0) {
            echo '<br />' . $error . '<br />';
        }

        

        if(!is_null($result) && $errno == 0) {
            $valores = json_decode($result, false);
            
            if(!isset($valores->{'error'})) {
                
                return $valores;
            }
        }
        return NULL;
    }

    public function recogerDatosPOST($url, $datos) {
        $cliente = $this->getPOSTConfiguration($url);

        $datosJson = [];
        //si nos han pasado un objeto y este implementa el methodo getData, obligatorio si impelemntas la interface ICurl, llamamos a ese methodo
        if(is_object($datos) && method_exists($datos, 'getData')) {
            $datosJson = $datos->getData();
            $datosJson = json_encode($datosJson);
        }
        else {
            $datosJson = json_encode($datos);
        }

        //print_r($datosJson);
        
        //añadimos los valores transformados a json
        curl_setopt($cliente, CURLOPT_POSTFIELDS, $datosJson);

        //ejecucion
        $result=curl_exec($cliente); //ejecutamos curl
        $errno = curl_errno($cliente); //ultimo codigo de error
        $error = curl_error($cliente); //devuelve una cadena con el ultimo codigo de error
        curl_close($cliente); //cerramos

        if($errno != 0) {
            echo '<br />' . $error . '<br />';
        }

        if(!is_null($result) && $errno == 0) {
            $valores = json_decode($result, false);
            if(!isset($valores->{'error'})) {
                return $result;
            }
        }

        return NULL;
    }

    public function login($nombreUsuario, $password) {

        $nombreUsuario = filter_var($nombreUsuario, FILTER_SANITIZE_STRING); //sanitizamos el nombre de usuario
        $password = hash("sha256", $password); //encriptamos el password

        $peticion = new PeticionLogin($nombreUsuario, $password);
        $peticion->setNombreUsuario($nombreUsuario);
        $peticion->setPassword($password);

        $resultado = json_decode($this->recogerDatosPOST(CurlControlador::$server . Usuario::$login, $peticion->getData()), false);
        //print_r($resultado);
        if(!is_null($resultado)) {
            $usuario = new Usuario();
            $usuario->addData($resultado);
            $_SESSION['login'] = json_encode($usuario->getData());
            //print_r($_SESSION['login']);
            return true;
        }
        else {
            return false;
        }
    }

    public function registrarse($nombreUsuario, $password, $mail, $nombre, $apellido, $segundoApellido, $fechaNacimiento) {
        
        $usuario = new Usuario();
        $usuario->setNombreUsuario(filter_var($nombreUsuario, FILTER_SANITIZE_STRING));
        $usuario->setPassword($password);
        $usuario->setMail(filter_var($mail, FILTER_SANITIZE_EMAIL));
        $usuario->setApellido(filter_var($apellido, FILTER_SANITIZE_STRING));
        $usuario->setSegundoApellido(filter_var($segundoApellido, FILTER_SANITIZE_STRING));
        $usuario->setNombre(filter_var($nombre, FILTER_SANITIZE_STRING));
        if($usuario->setFechaNacimiento($fechaNacimiento)) {
            $resultado = $usuario->guardar();

            if(!is_null($resultado)) {
                return true;
            }
        }
        
        return false;
    }
}

?>