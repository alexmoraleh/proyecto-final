var now=new Date();
var obj=[];
var meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

var urle = "";
if (screen.width < 460){
    urle = "../../tratarPeticiones.php";
} else {
    urle = "./tratarPeticiones.php"
}

$(document).ready(recibirdatos(0),$("#modal2").on('hide.bs.modal', function(){$("#mostrarmodal").modal("show");}), resize());
// Pace.stop();
$(document).ajaxStart(function() { Pace.restart()});
// $('#confirm-delete').on('show.bs.modal', function(e) {
//     $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
// });


// Para hacer responsive las celdas
$(window).resize(resize());

function resize(){
    for(var num=1;num<=6;num++){
        $("#sem"+ num +" .col").css("height", $(".col").css("width"));
        // $("#sem"+ num +" .col").css("height", "unset");
    }
}

$(function () {
    $('#datetimepicker1').datetimepicker();
});

function calendario(n){
    var p=$('#prueba');
    var first=new Date();
    var btnext=$('#btnext');
    var cont=1;
    // console.log(now.getDate() +" "+ now.getMonth() +" "+ now.getFullYear());
    // now.setDate(now-30);
    if(isNaN(n)){
        now=new Date();
    }
    else{
        if($('#numes').html()==20){
            // now.setDate(1);
            now.setMonth(now.getMonth()+n);
            now.setFullYear(now.getFullYear());
        }
        else{
            now.setDate(1);
            now.setFullYear(parseInt($('#numan').html(), 10));
            now.setMonth(parseInt($('#numes').html(), 10)+n);
        }
    }
    $('#numes').html(now.getMonth());
    $('#numan').html(now.getFullYear());
    first.setMonth(now.getMonth());
    first.setFullYear(now.getFullYear());
    // console.log(now.getMonth()+" "+first.getMonth());
    first.setDate(1);
    if(first.getDay()!=0){
        first.setDate(first.getDate()-first.getDay()+1);
    }
    else{
        first.setDate(first.getDate()-first.getDay()-6);
    }
    // console.log(meses[now.getMonth()]);
    $('#mestit').html(meses[now.getMonth()]+", "+now.getFullYear());
    // p.html(first.getDate());
    if(now.getMonth()==new Date().getMonth()){
        $('#hoy').addClass("selecfechoy");
        $('#hoy').attr("disabled", true);
    }
    else{
        $('#hoy').removeClass("selecfechoy");
        $('#hoy').attr("disabled", false);
    }
    $('span[id=nevent]').remove();
    while(cont<=7){
        $('#sem'+cont).children().each(function(){
            $(this).attr("onClick", "");
            var nevent=0;
            if(obj!=null){
                obj.forEach(element => {
                    // console.log(element.nombreUsuario);
                    if(element.dia==first.getDate() && (element.mes-1)==first.getMonth()){
                        nevent++;
                    }
                });
            }
            if(nevent!=0){
                $(this).append("<span id='nevent' title='Hay "+nevent+" eventos este dia'>"+nevent+"</span>");
                $(this).attr("onClick", "rellenarmodal(this)");
                $(this).attr("title", "Ver los eventos");
                $(this).attr("style", "cursor:pointer");
            }
            
            $(this).children().each(function(){
                if($(this).is("h5")){
                    $(this).removeClass("h5today finde otromes");
                    if (first.getDate()==now.getDate() && first.getMonth()==new Date().getMonth() && first.getFullYear()==new Date().getFullYear()){
                        $(this).addClass("h5today");
                        $(this).attr("title", "Hoy");
                    }
                    else{
                        // console.log(first.getDay());
                        if((first.getDay()==6||first.getDay()==0) && first.getMonth()==now.getMonth()){
                            $(this).addClass("finde");
                        }
                        else{
                            if(first.getMonth()!=now.getMonth()){
                                // console.log(now.getMonth()+" "+first.getMonth());
                                $(this).addClass("otromes");
                            }
                        }

                    }
                    $(this).html(first.getDate());
                    // console.log(first.getDate());
                    first.setDate(first.getDate()+1);
                }
            });
        });
        
        cont++;
    }
}
function recibirdatos(n){
    // var now=new Date();
    $('#loadevents').html("Cargando eventos...");
    if(isNaN(n)){
        now=new Date();
    }
    else{
        if($('#numes').html()==20){
            now.setMonth(now.getMonth()+n);
            now.setFullYear(now.getFullYear());
        }
        else{
            now.setFullYear(parseInt($('#numan').html(), 10));
            now.setMonth(parseInt($('#numes').html(), 10)+n);
        }
    }
    var mes1=now.getMonth();
    var mes2=now.getMonth();
    var mes3=now.getMonth();
    var ano1=now.getFullYear();
    var ano2=now.getFullYear();
    var ano3=now.getFullYear();
    if(mes1==0){
        mes1=11;
        mes3++;
        ano1--;
    }
    else{
        if(mes1==11){
            mes1--;
            mes3=0;
            ano3++;

        }
        else{
            mes1--;
            mes3++;
        }
    }
    
    //./../tratarPeticiones.php
    // console.log(mes1+" "+mes2+" "+mes3+" "+ano1+" "+ano2+" "+ano3);
    $.ajax({
        url : urle,
        data : {"mes1" : mes1 ,"ano1": ano1, "mes2" : mes2 ,"ano2": ano2, "mes3" : mes3 ,"ano3": ano3},
        type : 'POST',
        // el tipo de información que se espera de respuesta
        dataType : 'json',
        success : function(data) {
            // console.log(data);
            obj=data;
            
            $('#loadevents').html("");
            calendario(n);
            // data.forEach(element => {
            //     console.log(element.nombreUsuario);
            // });
        },
        error : function(jqXHR, status, error) {
            alert('No se han podido cargar los eventos');
            calendario(n);
        }
    });
}
function rellenarmodal(n){
    var dia=$(n).children(":first").html();
    var mes=$('#numes').html();
    var ano=$('#numan').html();
    $('#mostrarmodal div div .modal-header h3').html(dia+" "+meses[mes]+" "+ano);
    if(mes==20){
        mes=now.getMonth();
    }
    else{
        if(mes==0){
            mes=12;
        }
        else{
            mes++;
        }
    }
    if(ano==20){
        ano=now.getFullYear();
    }
    $('#mostrarmodal div div .modal-body').empty();
    obj.forEach(element => {
        // console.log(element.nombreUsuario);
        // console.log(element.dia+" "+element.mes+" "+element.ano);
        // console.log(dia+" "+mes+" "+ano);
        if(element.dia==dia && element.mes==mes && element.ano==ano){
            if(element.tipo=="ABRIR"){
                $('#mostrarmodal div div .modal-body').append("<div><p onClick='modal2("+element.id+")'>"+element.hora+ "0 <span style='color: var(--green)'>" + element.tipo + "</span> puerta" +"</p><button type='button' class='btn btn-outline-danger' onClick='preliminar("+ element.id +")' data-toggle='modal' data-target='#confirm-delete'>Eliminar</button></div>");
            }
            else{
                $('#mostrarmodal div div .modal-body').append("<div><p onClick='modal2("+element.id+")'>"+element.hora+ "0 <span style='color: var(--danger)'>" + element.tipo + "</span> puerta" +"</p><button type='button' class='btn btn-outline-danger' onClick='preliminar("+ element.id +")' data-toggle='modal' data-target='#confirm-delete'>Eliminar</button></div>");
            }
            $('#mostrarmodal div div .modal-body').append("<hr style='margin-top: 1px; margin-bottom:1px' />");
        }
    });
    $('#mostrarmodal div div .modal-body hr').last().remove();
    if($('#mostrarmodal div div .modal-body').is(':empty')){
        $('#mostrarmodal div div .modal-body').append("<p>Ve al mes correcto</p>");
    }
    
    $("#mostrarmodal").modal("show");
}
function modal2(n){
    $("#mostrarmodal").modal("hide");
    var mes=$('#numes').html();
    var habitaciones="";
    $('#modal2 div div .modal-body').empty();
    obj.forEach(element => {
        if(element.id==n){
            // console.log(element.id);
            $('#modal2 div div .modal-header h3').html(element.dia+" "+meses[mes]+"  "+element.hora+"0");
            if(element.tipo=="ABRIR"){
                $('#modal2 div div .modal-body').append("<p>La puerta se <span style='color: var(--green)'>abrirá</span>.</p>");
            }
            else{
                $('#modal2 div div .modal-body').append("<p>La puerta se <span style='color: var(--danger)'>cerrará</span>.</p>");
            }
            $('#modal2 div div .modal-body').append("<hr style='margin-top: 1px; margin-bottom:1px' />");
            $('#modal2 div div .modal-body').append("<div><p>Usuario: </p><p> "+ element.nombreUsuario +"</p></div>");
            $('#modal2 div div .modal-body').append("<hr style='margin-top: 1px; margin-bottom:1px' />");
            element.numHabitacion.forEach(e => {
                habitaciones=+e+", ";
            });
            $('#modal2 div div .modal-body').append("<div><p>Celdas: </p> <p> "+ element.numHabitacion +"</p></div>");
            $('#modal2 div div .modal-body').append("<hr style='margin-top: 1px; margin-bottom:1px' />");
            $('#modal2 div div .modal-body').append("<p>Descripción:</p>");
            $('#modal2 div div .modal-body').append("<texarea rows='4' cols='50'>"+ element.descripcion +"</textarea>");
            $('#modal2 div div .modal-body').append("<br><button type='button' class='btn btn-outline-danger' onClick='preliminar("+ element.id +")' data-toggle='modal' data-target='#confirm-delete'>Eliminar</button>");
        }
    });
    // $('#modal2 div div .modal-body hr').last().remove();
    $('#modal2').modal("show");
}
function createvent(){
    $('.fa-plus').css("transform","rotate(45deg)");
    $('.fa-plus').css("z-index","9999");
    $('.fa-plus').attr("onClick","fapluscerrar()");
    // $('#dpick div div .modal-body').empty();
    $('#dpick div div .modal-header h3').html("Creación de evento");
    // $('#dpick div div .modal-body').append("<p>Prueba</p>");
    $('#datetimepicker1').datetimepicker();
    $('#dpick').modal("show");
}
function fapluscerrar(){
    $('.fa-plus').attr("onClick","createvent()");
    $('#dpick').modal("hide");

    setTimeout(function(){
        $('.fa-plus').css("z-index","inherit");
        $('.fa-plus').css("transform","");
    },300);
    
}

// Esto esta a parte porque no puedo usar lo comentado ya que le estaria diciendo que al cerrarse se cierre y es un bucle infinito
$("#dpick").on('hide.bs.modal', function(){
    $('.fa-plus').attr("onClick","createvent()");
    // $('#dpick').modal("hide");

    setTimeout(function(){
        $('.fa-plus').css("z-index","inherit");
        $('.fa-plus').css("transform","");
    },300);
});
function crearevento(){
    var cont=0;
    var dia="";
    var mes="";
    var ano="";
    var hor="";
    var min="";
    var tipo="";
    var celdas=[];
    var aux=[];
    var desc=";"
    if($('#fdatepicker').val()==""||$('#ftimepicker').val()==""){
        alert('Inserte una fecha');
        return;
    }
    if($('#option1').is(":checked")){
        tipo="ABRIR";
    }
    else{
        if($('#option2').is(":checked")){
            tipo="CERRAR";
        }
        else{
            alert('Elija la acción');
            return;
        }
    }
    if($('#fceldasinput').val()==""){
        alert('Escriba alguna celda');
        return;
    }
    if($('#ftextarea').val()==""){
        alert('Escriba alguna descripción');
        return;
    }
    aux=$('#fdatepicker').val().split("/");
    dia=aux[0];
    mes=aux[1];
    ano=aux[2];
    aux=$('#ftimepicker').val().split(":");
    hor=aux[0];
    min=aux[1];
    celdas=$('#fceldasinput').val().split(",");
    celdas.forEach(e => {
        celdas[cont]=e.trim();
        cont++;
    });
    desc=$('#ftextarea').val();
    // console.log(dia+" "+mes+" "+ano+" "+hor+" "+min);
    // Pace.restar();
    $.ajax({
        url : urle,
        data : {"dia" : dia ,"mes": mes, "ano" : ano ,"hora": hor, "minuto" : min ,"segundo": 00, "numHabitacion" : celdas, "descripcion" : desc, "tipo" : tipo},
        type : 'POST',
        async : false,
        // el tipo de información que se espera de respuesta
        // dataType : 'json',
        success : function(data) {
            if(data){
                $('#dpick div div .modal-body').prepend("<div class='alert alert-success alert-dismissible fade show' role='alert'>Evento creado correctamente.<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>");
                $('#fdatepicker').val("");
                $('#ftimepicker').val("");
                $('#option1').attr("checked","false");
                $('#option2').attr("checked","false");
                $('#fceldasinput').val("");
                $('#ftextarea').val("");
                setTimeout(function(){
                    calendario("nuevo");
                },100);
            }
            else{
                $('#dpick div div .modal-body').prepend("<div class='alert alert-danger alert-dismissible fade show' role='alert'>Ha habido un error, no se ha creado el evento.<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>");
                setTimeout(function(){
                    calendario("nuevo");
                },100);
            }
        },
        error : function(jqXHR, status, error) {
            $('#dpick div div .modal-body').prepend("<div class='alert alert-danger alert-dismissible fade show' role='alert'>Ha habido un error, no se ha creado el evento. Fallo de comunicación.<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>");
        }
    });
}
function preliminar(n){
    $('#confirm-delete div div .modal-footer a').attr("onClick", "eliminar("+ n +")");
}
function eliminar(n){
    var modal="";
    if($('#mostrarmodal').hasClass('show')){
        modal="mostrarmodal";
    }
    else{
        modal="modal2";
    }
    // alert("Borrando "+n);
    $.ajax({
        url : urle,
        data : {"id" : n, "borrar" : true},
        type : 'POST',
        success : function(data){
            if(data){
                $('#'+ modal +' div div .modal-body').prepend("<div class='alert alert-success alert-dismissible fade show' role='alert'>Evento eliminado correctamente.<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>");
                setTimeout(function(){
                    calendario("nuevo");
                },100);
            }
            else{
                $('#'+ modal +' div div .modal-body').prepend("<div class='alert alert-danger alert-dismissible fade show' role='alert'>Ha habido un error, no se ha eliminado el evento.<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>");
                setTimeout(function(){
                    calendario("nuevo");
                },100);
            }
        },
        error : function(jqXHR, status, error) {
            $('#'+ modal +' div div .modal-body').prepend("<div class='alert alert-danger alert-dismissible fade show' role='alert'>Ha habido un error, no se ha eliminado el evento. Fallo de comunicación.<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>");
        }
    });
    $('#confirm-delete').modal("hide");
}