<?php

    class Recluso implements ICurl {
        public static $add = 'recluso/add';
        public static $get = 'recluso/get?id=';
        public static $getAll = 'recluso/getAll';
        public static $gdeleteet = 'recluso/delete?id=';
        public static $getMovimientos = 'recluso/getMovimientos?id=';
        public static $getAsignaciones = 'recluso/getAsignaciones?id=';

        private $id;
        private $nombre;
        private $apellido;
        private $segundoApellido;
        private $fechaNacimiento;
        private $dni;
        private $pasaporte;
        private $imagenPreso;
        private $menor;
        private $extranjero;
        private $codigoNacionalidad;
        private $sexo;
        private $fechaAlta;
        private $motivoAlta; //enum
        private $otrosMotivosAlta;
        private $fechaBaja;
        private $motivoBaja;//enum
        private $otrosMotivosBaja; 

        function __construct() {
            $this->fechaAlta = str_replace(' ', 'T', date('Y-m-d H:m:s.000O')); //añadimos la fecha de alta actual
            $this->menor = false;
            $this->extranjero = false;
        }

        public function getId() {
            return $this->id;
        }

        public function getNombre() {
            return $this->nombre;
        }

        public function setNombre($nombre) {
            $this->nombre = $nombre;
        }

        public function getApellido() {
            return $this->apellido;
        }

        public function setApellido($apellido) {
            $this->apellido = $apellido;
        }

        public function getSegundoApellido() {
            return $this->segundoApellido;
        }

        public function setSegundoApellido($apellido) {
            $this->segundoApellido = $apellido;
        }

        public function getFechaNacimiento() {
            return $this->fechaNacimiento;
        }

        public function setFechaNacimiento($fecha) {
            $this->fechaNacimiento = $fecha;
        }
        //
        public function getDni() {
            return $this->nombre;
        }

        public function setDni($dni) {
            $this->dni = $dni;
        }

        public function getPasaporte() {
            return $this->pasaporte;
        }

        public function setPasaporte($pasaporte) {
            $this->pasaporte = $pasaporte;
        }

        public function getImagenPreso() {
            return $this->imagenPreso;
        }

        public function setImagenPreso($imagen) {
            $this->imagenPreso = $imagen;
        }

        public function getMenor() {
            return $this->menor;
        }

        public function setMenor($menor) {
            $this->menor = $menor;
        }
        //
        public function getExtranjero() {
            return $this->extranjero;
        }

        public function setExtranjero($extranjero) {
            $this->extranjero = $extranjero;
        }

        public function getCodigoNacionalidad() {
            return $this->codigoNacionalidad;
        }

        public function setCodigoNacionalidad($nacionalidad) {
            $this->codigoNacionalidad = $nacionalidad;
        }

        public function getSexo() {
            return $this->sexo;
        }

        public function setSexo($sexo) {
            $this->sexo = $sexo;
        }

        public function getFechaAlta() {
            return $this->fechaAlta;
        }

        public function setFechaAlta($fecha) {
            $this->fechaAlta = $fecha;
        }

        public function getMotivoAlta() {
            return $this->motivoAlta;
        }

        public function setMotivoAlta($motivo) {
            $this->motivoAlta = $motivo;
        }

        public function getOtrosMotivosAlta() {
            return $this->otrosMotivosAlta;
        }

        public function setOtrosMotivosAlta($motivo) {
            $this->otrosMotivosAlta = $motivo;
        }

        public function getFechaBaja() {
            return $this->fechaBaja;
        }

        public function setFechaBaja($fecha) {
            $this->fechaBaja = $fecha;
        }

        public function getMotivoBaja() {
            return $this->motivoBaja;
        }

        public function setMotivoBaja($motivo) {
            $this->motivoBaja = $motivo;
        }

        public function getOtrosMotivosBaja() {
            return $this->otrosMotivosBaja;
        }

        public function setOtrosMotivosBaja($motivo) {
            $this->otrosMotivosBaja = $motivo;
        }

        public function toString() : String {
            $cadena = 'Recluso: id: ' . $this->id . ' nombre: ' . $this->nombre . ' apellido: ' . $this->apellido . ' segundoApellido: ' . $this->segundoApellido;
            return $cadena;
        }

        public function guardar() {
            $curl = CurlControlador::Singleton();
            $resultado = $curl->recogerDatosPOST(CurlControlador::$server . Recluso::$add, $this->getData());

            if(is_null($resultado)) {
                return NULL;
            }
            else{
                if(is_numeric($resultado)) {
                    if($resultado > 0) {
                        $this->id = $resultado;
                    }
                    return $resultado;
                }
                else {
                    return NULL;
                }
            }
        }

        //convierte el objeto a una array con el contenido de las variables a json
        public function getData() : Array
        {
            $var = get_object_vars($this);
            foreach($var as $value => &$val) {
                if (is_object($val) && method_exists($val,'getData')) {
                    $val = $val->getData();
                }
                elseif(is_array($val)) {
                    $array = [];
                    foreach($val as $localDate) {
                        if(is_object($localDate) && method_exists($localDate, 'getData')) {
                            array_push($array, $localDate->getData());
                        }
                        else {
                            array_push($array, $localDate);
                        }
                    }
                    $val = $array;
                }
            }
            return $var;
        }

        //añade los datos a este objeto
        public function addData($data)
        {
            foreach($data as $key => $value) {
                switch($key) {
                    default:
                        $this->{$key} = $value;
                }
            }
        }
    }


?>