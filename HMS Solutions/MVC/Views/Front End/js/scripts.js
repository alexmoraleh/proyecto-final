var nums = true;
window.onscroll = function() {scrollFunction(), cards(), logo(), numeros(), btnhome()};

$(document).ready(height(), subtitulo(), footer());

//Correción margin-top slogan
  function subtitulo(){
    // if (document.documentElement.scrollTop < 50){
    //   $('.leteffsub').css("margin-top", $('#logomain').offset().top);
    // } (commentado porque esta solucionado, creo )
    if($(window).width()>500){
      $('.leteffsub').css("margin-top", $('#logomain').offset().top);
    }
  }

//Aparece footer
  function footer(){
    // $('body').css("height", $('#footer').css("height")+$('#section').css("height"));
    // console.log($('#footer').css("height")+$('#section').css("height"));
    // setTimeout(function(){
    //   document.getElementById("body").style.height = (document.getElementById("section").offsetHeight+310) + document.getElementById("footer").offsetHeight+"px";
    //   console.log(document.getElementById("prueba").offsetHeight);
    // }, 100);
    // console.log(document.getElementById("section").offsetHeight);
    $('section').css("margin-bottom", $('footer').css("height"));
  }

//Efecto menú
  function scrollFunction() {
    var menu=document.getElementById("menu");
    var logo=document.getElementById("logo");
    if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
      //   document.getElementById("menu").style.width = "90%";  
      menu.style.height = "80px";
      menu.style.backgroundColor="rgba(255, 255, 255, 0.95)";
      logo.style.width = "60px";
      menu.style.width = "100%";
      menu.style.boxShadow = "0px 0px 38px -3px rgba(0,0,0,0.5)";
  //      document.getElementById("falsomenu").style.boxShadow = "none";
      
    } else {
      // document.getElementById("menu").style.width = "100%";
      menu.style.height = "100px";
      menu.style.backgroundColor="transparent";
      logo.style.width = "80px";
      menu.style.width = "90%";
      menu.style.boxShadow = "none";
  //    document.getElementById("falsomenu").style.boxShadow = "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)";
    }
    if (document.body.scrollTop > 55 || document.documentElement.scrollTop > 55) {
      menu.style.height = "80px";
    }
    else{
      menu.style.height = "100px";
    }
  }

//Transformacion logo
  function logo(){
    var logo=document.getElementById("logomain");
      if (document.body.scrollTop > 300 || document.documentElement.scrollTop > 300 ) {
      logo.className="letefftitmen";
      console.log("arriba");
    } else {
      if(document.body.scrollTop < 1 || document.documentElement.scrollTop < 1 && logo.className!="letefftit"){
        logo.className="letefftit";
        $('.scroll-down').css("top", "inherit");
        console.log("abajo");
      }
    }
    if (document.body.scrollTop > 250 || document.documentElement.scrollTop > 250 ) {
      $('.scroll-down').css("top", "0");
    }
  }

// "Transformacion" boton home
  function btnhome(){
    var btn=document.getElementById("btnmen");
    if (document.body.scrollTop > ($(".btntit").offset().top+50) || document.documentElement.scrollTop > ($(".btntit").offset().top+50) ) {
      btn.style.filter="opacity(1)";
      btn.style.marginRight="5px";
    }
    else{
      btn.style.filter="opacity(0)";
      btn.style.marginRight="50%";
    }
  }

//Aparecen las tarjetas
  function cards(){
    if (document.body.scrollTop > ($(".card").offset().top - 800) || document.documentElement.scrollTop > ($(".card").offset().top - 800) && $(".card")[0].style.visibility!="visible") {
      var cards=$(".card");
      let index = 0;
      let interval = setInterval(function() {
        let card = cards[index];
        card.style.visibility = "visible";
        card.style.filter = "opacity(1)";
        card.style.marginTop = "20px";
        index++;
        if(index==1){
          setTimeout(function(){
            $(".card:first-child").css("background-color","rgba(255, 255, 255, 0.4)");
            setTimeout(function(){
              $(".card:first-child").css("backgorund-color","rgba(255, 255, 255, 0.9)");
              console.log("hola");
            }, 500);
          },100);
        }
        if (index >= cards.length) {
          // console.log(interval);
          clearInterval(interval);
          // console.log("cleared");
        }
      }, 500);
    }
  }

//cambiar height del home
  function height(){
    $('.homecont').css("height",$(window).height());
    $('.homecont').css("background-size","auto "+($(window).height()+150)+"px");
    if($(window).width()<500){
      $('.homecont').css("background-size","auto "+($(window).height()+250)+"px");
    }
  }

// Mover el background
  $(document).ready(function() {
    var movementStrength = 15;
    var height = movementStrength / $(window).height();
    var width = movementStrength / $(window).width();
    if($(window).width()>500){
      $(".homecont").mousemove(function(e){
                var pageX = e.pageX - ($(window).width() / 8);
                var pageY = e.pageY - ($(window).height() / 7);
                var newvalueX = width * pageX * -1 - 25;
                var newvalueY = height * pageY * -1 - 50;
                $('.homecont').css("background-position", newvalueX+"px     "+newvalueY+"px");
      });
    }
  });

//Contador de 0 a n
  function numeros(){
    if((document.body.scrollTop > ($(".cont").offset().top - 1000) || document.documentElement.scrollTop > ($(".cont").offset().top - 1000)) && nums==true) {
      console.log("dentro");
      // $('.numvis').each(function() {
      //   console.log($(this).text);
      //   $(this).style.marginTop="0px";
      //   $(this).style.visibility="visible";
      // });
      $('.numvis').css("margin-top","0px");
      $('.numvis').css("margin-bottom","100px");
      $('.numvis').css("filter","opacity(1)");
      $('.cont').each(function () {
        $(this).prop('Counter',0).delay(800).animate({
            Counter: $(this).text()
        }, {
            
            duration: 2000,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.round(now * 100) / 100);
          }
        });
      });
      nums=false;
    }
  }

// Scroll down
  $(function() {
    $('.scroll-down').click (function() {
      $('html, body').animate({scrollTop: $('#info').offset().top - 100 }, 'slow');
      return false;
    });
  });

// ===== Scroll to Top ==== 
$(window).scroll(function() {
  if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
      $('#return-to-top').fadeIn(200);    // Fade in the arrow
  } else {
      $('#return-to-top').fadeOut(200);   // Else fade out the arrow
  }
});
$('#return-to-top').click(function() {      // When arrow is clicked
  $('body,html').animate({
      scrollTop : 0                       // Scroll to top of body
  }, 500);
});

//Letras de home
// $(document).ready(function(){
//   var mouseX, mouseY;
//   var ww = $( window ).width();
//   var wh = $( window ).height();
//   var traX, traY;
//   $(document).mousemove(function(e){
//     mouseX = e.pageX;
//     mouseY = e.pageY;
//     traX = ((4 * mouseX) / 570) + 40;
//     traY = ((4 * mouseY) / 570) + 50;
//     //console.log(traX);
//     $(".letefftit").css({"background-position": traX + "%" + traY + "%"});
//   });
// });
