<?php
  if(file_exists('./MVC/Controller/YouAreLogged.php')) {
    require_once('./MVC/Controller/YouAreLogged.php');
  }
  else {
    header('Location: ../../index.php');
  }

?>


<div class="body-container container">
    <div class="zonas-container" id="zonas"> 

    </div>
    <div class="celldas-container" id="allCeldas">
        <div id="celdas-container">
            <div class="botones" id="botones">
            
            </div>
            <div class="celdas" id="celdas">

            </div>
        </div>
    </div>
    <div class="popups" id="popups">

    </div>
</div>

<div class="modal fade" id="confirm-action" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" id="confirmacionHeader">
                Abrir zona
            </div>
            <div class="modal-body" id="confirmacionBody">
                ¿Esta seguro que desea abrir esta zona?
            </div>
            <div class="modal-footer" id="confirmacionFooter">
                <button type="button" class="btn btn-default" data-dismiss="modal" onClick="">Continuar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<script>
    $.ajax(
    {
        type: "POST",
        url: "./tratarPeticiones.php",
        data: {"zonas": true},
        success: function(data) {
            //console.log(JSON.parse(data)[0]);
            eliminarNodes("zonas-container");
            let i = 0;
            let div = null;
            JSON.parse(data).forEach(element => {
                if(i == 0) {
                    div = document.createElement('div');
                    div.className += 'dosZonas';
                }
                div.appendChild(crearModulo(element.id, element.edificio, element.planta, element.nombre, element.descripcion, element.estado));
                
                i++;
                if(i == 2) {
                    i = 0;
                    apendar(div, 'zonas');
                }
            });  
            apendar(div, 'zonas');
        },
        error: function(){
            console.log("error");
        }
    });

    function getCeldas(zonaId) {
        //console.log(zonaId);
        $.ajax(
        {
            type: "POST",
            url: "./tratarPeticiones.php",
            data: {"zonaId": zonaId},
            success: function(data) {
                let zonas = document.getElementById('zonas');
                zonas.style = 'width: 40%';
                let celdas = document.getElementById('allCeldas');
                celdas.style = 'width: 58%';
                
                eliminarNodes("celdas-container");

                let botones = document.createElement('div');
                botones.className += 'botones';
                botones.id = 'botones';
                botones.innerHTML = '<i class="fas fa-lock-open openCeldaButton" onclick="prepararConfirmacionZona(' + zonaId + ', 0)"></i>' +
                '<i class="fas fa-lock closeCeldaButton" onclick="prepararConfirmacionZona(' + zonaId + ', 1)"></i>';

                apendar(botones, 'celdas-container');

                let celdas2 = document.createElement('div');
                celdas2.className +=  'celdas';
                celdas2.id = 'celdas';

                apendar(celdas2, 'celdas-container');

                let i = 0;
                let e = null;
                JSON.parse(data).forEach(element => {
                    if(i == 0) {
                        e = document.createElement('div');
                        e.className += 'dosZonas';
                    }

                    e.appendChild(dibujarCelda(element));
                    apendar(dibujarPopUp(element), 'popups');

                    i++;
                    if(i == 2) {
                        i = 0;
                        apendar(e, 'celdas');
                    }
                });
                if(e != null) apendar(e, 'celdas');
            },
            error: function(){
                console.log("error");
            }
        });
    }

</script>