$(document).ajaxStart(function() { Pace.restart()});

function eliminarNodes(padre) {
	$('#'+padre).children().each(function () {
		$(this).remove();
	});
}

/*añade un modulo*/
function crearModulo(id, edificio, planta, zona, descripcion, estado) {
	let newStatusBox = document.createElement('div');
	newStatusBox.className += 'status-box';
    newStatusBox.id = id;
    
    newStatusBox.addEventListener('click', function() { //añade un onclick el qual llama a ver log y le pasa su id
        getCeldas(this.id); //ojo, cambiar
    });

	let presentacion = document.createElement('div');
	presentacion.className += 'status-presentacion-box';
	estilo = null;
	switch(estado) {
		case 'WARNING':
				presentacion.style='background-color: var(--keys)';
		break;
		case 'ATENTION':
			presentacion.style='background-color: var(--keyClose)';
		break;
		default:
			presentacion.style='background-color: var(--jeyOpen)';
	}

	presentacion.innerHTML = '<span class="iniciales">' + getIniciales(zona) + '</span>' +
	'<br /><span class="nombre">' + zona + '</span>' ;

	let statusbox = document.createElement('div');
	statusbox.className += 'status-info-box';
	statusbox.innerHTML = '<span><u>Edificio:</u> ' + edificio + ' <u>Planta:</u> ' + planta + '</span>'+
    '<span><u>Descripción:</u> ' + descripcion +'</span>';

	newStatusBox.appendChild(presentacion);
	newStatusBox.appendChild(statusbox);

	return newStatusBox;
}

function getIniciales(cadena) {
	separador = " ", // un espacio en blanco
	arregloDeSubCadenas = cadena.split(separador); // SEPARA EL NOMBRE EN CADENAS INDIVIDUALES
	resultado = '';
    // IMPRIME LA PRIMERA LETRA DE CADA CADENA 
    for (x=0;x<arregloDeSubCadenas.length;x++){
        subCadena = arregloDeSubCadenas[x].substring(0, 1);
        resultado += subCadena.toUpperCase() + ". ";
	}
	return resultado;
}

function dibujarCelda(elemento) {
	let padre = document.createElement('div');
	padre.className += 'celda';
	padre.id = 'C-' + elemento.id;

	padre.addEventListener('click', function() { //añade un onclick el qual llama a ver log y le pasa su id
        verPopUp(this.id + '-P'); //ojo, cambiar
    });

	if(elemento.estadoPuerta == 'CERRADA') {
		padre.innerHTML = '<i class="fas fa-lock closeCeldaButton"></i>';
		padre.innerHTML += '<p class="celdaNum">' + elemento.numHabitacion + '</p>';
	}
	if(elemento.estadoPuerta == 'ABIERTA') {
		padre.innerHTML = '<i class="fas fa-lock-open openCeldaButton"></i>';
		padre.innerHTML += '<p class="celdaNum">' + elemento.numHabitacion + '</p>';
	}

	return padre;
}

function dibujarPopUp(elemento) {

	let popUp = document.createElement('div');
	popUp.className += 'modal fade';
	popUp.id = 'C-' + elemento.id + '-P';

	let dialogo = document.createElement('div');
	dialogo.className += 'modal-dialog';

	let content = document.createElement('div');
	content.className += 'modal-content';

	let header = document.createElement('div');
	header.className += 'modal-header';

	let info = document.createElement('div');
	info.className += 'presentacion';

	if(elemento.estadoPuerta == 'CERRADA') {
		header.innerHTML = '<div class="presentacion"><span><i class="fas fa-lock closeCeldaButton"></i></span> <span>' + elemento.numHabitacion + '</div>';
	}
	if(elemento.estadoPuerta == 'ABIERTA') {
		header.innerHTML = '<div class="presentacion"><span><i class="fas fa-lock-open openCeldaButton"></i></span> <span>' + elemento.numHabitacion + '</div>';
	}

	header.innerHTML += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
	content.appendChild(header);

	let bodyP = document.createElement('div');
	bodyP.className += 'modal-body';

	let infoCelda = document.createElement('div');
	infoCelda.className += 'infoCelda';

	infoCelda.innerHTML = '<span><u>Tipo Habitación:</u> ' + elemento.tipoHabitacion.nombreTipo + '</span><br />' +
	'<span><u>Tipo Descripción:</u> ' + elemento.tipoHabitacion.descripcionTipo + '</span><br />' +
	'<span><u>Capacidad de reclusos:</u> ' + elemento.tipoHabitacion.capacidad + '</span>';

	bodyP.appendChild(infoCelda);

	let reclusos = document.createElement('div');
	reclusos.className += 'reclusos';
	reclusos.innerHTML = '<u>Reclusos:</u>';

	let contenidoRecluso = document.createElement('div');
	contenidoRecluso.className += 'contenidoReclusos';

	let headerRecluso = document.createElement('div');
	headerRecluso.className += 'header-recluso';

	let data0 = document.createElement('div');
	data0.className += 'header-data';
	data0.id = 'numColumnaRecluso';
	data0.innerHTML = '<u>Número recluso</u>';
	
	let data1 = document.createElement('div');
	data1.className += 'header-data';
	data1.id = 'nombreColumnaRecluso';
	data1.innerHTML = '<u>Nombre recluso</u>';

	let data2 = document.createElement('div');
	data2.className += 'header-data';
	data2.id = 'fechaAsignacionColumnaRecluso';
	data2.innerHTML = '<u>Fecha de Asignación</u>';

	headerRecluso.appendChild(data0);
	headerRecluso.appendChild(data1);
	headerRecluso.appendChild(data2);

	contenidoRecluso.appendChild(headerRecluso);

	let body = document.createElement('div');
	body.className += 'reclusos-datos';

	elemento.asignaciones.forEach(function(datos) {
		let fila = document.createElement('div');
		fila.className += 'filaReclusos';

		let num = document.createElement('div');
		num.className += 'numeroRecluso';
		num.innerHTML = datos.recluso.id;
		
		let nombre = document.createElement('div');
		nombre.className += 'reclusoNombre';
		nombre.innerHTML = datos.recluso.apellido + ' ' + datos.recluso.segundoApellido + ', ' + datos.recluso.nombre

		let fecha = document.createElement('div');
		fecha.className += 'fechaRecluso';
		fecha.innerHTML = datos.fechaRealizacion.replace('T', ' ').replace('.000+0000', '');

		fila.appendChild(num);
		fila.appendChild(nombre);
		fila.appendChild(fecha);
		body.appendChild(fila);
	});

	contenidoRecluso.appendChild(body);
	reclusos.appendChild(contenidoRecluso);
	bodyP.appendChild(reclusos);

	let footer = document.createElement('div');
	footer.className += 'popFooter';

	let footButtons = document.createElement('div');
	footButtons.className += 'popBotones';

	footButtons.innerHTML = '<i class="fas fa-lock-open openCeldaButton" onclick="prepararConfirmacionHabitacion(\'' + elemento.numHabitacion + '\', 0, ' + elemento.zona.id + ', \'C-' + elemento.id + '-P\')"></i>' + 
	'<i class="fas fa-lock closeCeldaButton" onclick="prepararConfirmacionHabitacion(\'' + elemento.numHabitacion + '\', 1, ' + elemento.zona.id + ', \'C-' + elemento.id + '-P\')"></i>';

	footer.appendChild(footButtons);

	bodyP.appendChild(footer);
	content.appendChild(bodyP);
	dialogo.appendChild(content);
	popUp.appendChild(dialogo);

	return popUp;
}

function verPopUp(id) {
	$('#' + id).modal('show');
}

function apendar(elemento, idPadre) {
	document.getElementById(idPadre).appendChild(elemento);
}

function openDoor(celdaNum, accion, zonaId, id) {
	//console.log('openDoor');
	$.ajax(
	{
		type: "POST",
		url: "./tratarPeticiones.php",
		data: {"operacionPuerta": accion, "openCeldaNum": celdaNum},
		success: function(data) {
			if(JSON.parse(data).res == true) {
				getZonas();
				getCeldas(zonaId);
				$('#allCeldas').prepend("<div class='alert alert-success alert-dismissible fade show' role='alert'>Operación realizada correctamente.<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>");
			}
			else {
				$('#allCeldas').prepend("<div class='alert alert-warning alert-dismissible fade show' role='alert'>No ha sido posible realizar la acción solicitada.<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>");
			}
			$('#' + id).modal('toggle');
		},
		error: function(xhr, ajaxOptions, thrownError){
			console.log("error");
			$('#allCeldas').prepend("<div class='alert alert-warning alert-dismissible fade show' role='alert'>Ha habido un error durante la operación.<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>");
		}
	});
}

function openZona(zonaId, abrir) {
	$.ajax(
	{
		type: "POST",
		url: "./tratarPeticiones.php",
		data: {"operacionPuerta": abrir, "openZonaId": zonaId},
		success: function(data) {
			if(JSON.parse(data).res == true) {
				getZonas();
				getCeldas(zonaId);
				$('#allCeldas').prepend("<div class='alert alert-success alert-dismissible fade show' role='alert'>Operacion realizada correctamente.<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>");
			}
			else {
				$('#allCeldas').prepend("<div class='alert alert-warning alert-dismissible fade show' role='alert'>No ha sido posible realizar la acción solicitada.<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>");
			}
			
		},
		error: function(){
			console.log("error");
			
			$('#allCeldas').prepend("<div class='alert alert-warning alert-dismissible fade show' role='alert'>Ha habido un error durante la operacion.<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>");
		}
	});
}

function prepararConfirmacionHabitacion(habitacion, accion, zonaId, popUpId) {
	if(accion == 0) {
		document.getElementById('confirmacionHeader').innerHTML = 'Abrir habitación';
		document.getElementById('confirmacionBody').innerHTML = '¿Esta seguro que quiere abrir esta habitación?';
	}
	else {
		document.getElementById('confirmacionHeader').innerHTML = 'Cerrar habitación';
		document.getElementById('confirmacionBody').innerHTML = '¿Esta seguro que quiere cerar esta habitación?';
	}
	let botones = document.getElementById('confirmacionFooter');
	botones.innerHTML = '<button type="button" class="btn btn-default" data-dismiss="modal" onClick="openDoor(\'' + habitacion + '\', ' + accion + ', ' + zonaId + ', \'' + popUpId + '\')">Continuar</button>' +
	'<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>';
	verPopUp('confirm-action');
}

function prepararConfirmacionZona(zonaId, accion) {
	if(accion == 0) {
		document.getElementById('confirmacionHeader').innerHTML = 'Abrir Zona';
		document.getElementById('confirmacionBody').innerHTML = '¿Esta seguro que quiere abrir esta zona?';
	}
	else {
		document.getElementById('confirmacionHeader').innerHTML = 'Cerrar Zona';
		document.getElementById('confirmacionBody').innerHTML = '¿Esta seguro que quiere cerar esta zona?';
	}
	let botones = document.getElementById('confirmacionFooter');
	botones.innerHTML = '<button type="button" class="btn btn-default" data-dismiss="modal" onClick="openZona(' + zonaId + ', ' + accion + ')">Continuar</button>' +
	'<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>';
	verPopUp('confirm-action');
}

function getZonas() {
	$.ajax(
    {
        type: "POST",
        url: "./tratarPeticiones.php",
        data: {"zonas": true},
        success: function(data) {
            //console.log(JSON.parse(data)[0]);
            eliminarNodes("zonas");
            let i = 0;
            let div = null;
            JSON.parse(data).forEach(element => {
                if(i == 0) {
                    div = document.createElement('div');
                    div.className += 'dosZonas';
                }
                div.appendChild(crearModulo(element.id, element.edificio, element.planta, element.nombre, element.descripcion, element.estado));
                
                i++;
                if(i == 2) {
                    i = 0;
                    apendar(div, 'zonas');
                }
            });  
            apendar(div, 'zonas');
        },
        error: function(){
            console.log("error");
        }
	});
}