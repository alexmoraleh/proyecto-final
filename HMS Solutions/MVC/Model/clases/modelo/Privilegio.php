<?php

    class Privilegio implements ICurl {

        public static $add = 'privilegio/add';
        public static $getAll = 'privilegio/getAll';
        public static $get = 'privilegio/get?id=';
        public static $delete = 'privilegio/delete?id=';
        public static $getUsuarios = 'privilegio/getUsuarios?id=';
        public static $getModulos = 'privilegio/getModulos?id=';

        private $id;
        private $nombre;
        private $descripcion;
        private $fechaAlta;
        private $fechaBaja;

        function __construct() {
            $this->fechaAlta = str_replace(' ', 'T', date('Y-m-d H:m:s.000O')); //añadimos la fecha de alta actual
        }

        public function getId() {
            return $this->id;
        }

        public function getNombre() {
            return $this->nombre;
        }

        public function setNombre($nombre) {
            $this->nombre = $nombre;
        }

        public function getDescripcion() {
            return $this->descripcion;
        }

        public function setDescripcion($descripcion) {
            $this->descripcion = $descripcion;
        }

        public function getFechaAlta() {
            return $this->fechaAlta;
        }

        public function getFechaBaja() {
            return $this->fechaBaja;
        }

        public function setFechaBaja($fecha) {
            $this->fechaBaja = $fecha;
        }

        public function toString() : String {
            return 'Privilegio: id: ' . $this->getId() . ' nombre: ' . $this->getNombre() . ' descripcion: ' . $this->getDescripcion() . ' fechaAlta: ' . $this->getFechaAlta();
        }

        public function guardar() {
            
            $curl = CurlControlador::Singleton();
            $resultado = $curl->recogerDatosPOST(CurlControlador::$server . Privilegio::$add, $this->getData());

            if(is_null($resultado)) {
                return NULL;
            }
            else{
                if(is_numeric($resultado)) {
                    if($resultado > 0) {
                        $this->id = $resultado;
                    }
                    return $resultado;
                }
                else {
                    return NULL;
                }
            }
        }

        public function getData() : Array
        {
            $var = get_object_vars($this);
            foreach($var as $value => &$val) {
                if (is_object($val) && method_exists($val,'getData')) {
                    $val = $val->getData();
                }
                elseif(is_array($val)) {
                    $array = [];
                    foreach($val as $localDate) {
                        if(is_object($localDate) && method_exists($localDate, 'getData')) {
                            array_push($array, $localDate->getData());
                        }
                        else {
                            array_push($array, $localDate);
                        }
                    }
                    $val = $array;
                }
            }
            return $var;
        }

        public function addData($data)
        {
            foreach($data as $key => $value) {
                switch($key) {
                    default:
                        $this->{$key} = $value;
                }
            }
        } 
    }
?>