<?php
    //date_default_timezone_set("Europe/Madrid"); 

    //clases de acceso al web Service
    require_once('clases/internas/curl/CurlControlador.php');
    require_once('clases/internas/curl/interfaces/ICurl.php');

    //clases del modelo de base de datos
    require_once('clases/modelo/Privilegio.php');
    require_once('clases/modelo/Recluso.php');
    require_once('clases/modelo/Usuario.php');
    require_once('clases/modelo/Modulo.php');
    require_once('clases/modelo/Calendario.php');
    require_once('clases/modelo/TipoHabitacion.php');
    require_once('clases/modelo/MotivoAccionLogPuerta.php');
    require_once('clases/modelo/Zona.php');
    require_once('clases/modelo/Habitacion.php');
    require_once('clases/modelo/Asignacion.php');
    require_once('clases/modelo/LogPuerta.php');
    require_once('clases/modelo/Movimiento.php');
    require_once('clases/modelo/ReglaDeAccion.php');
    require_once('clases/modelo/Work.php');

    //clases de peticiones
    require_once('clases/peticiones/PeticionLogin.php');

    //peticiones internas
    require_once('clases/internas/internasDeLaInterface/Log.php');
    require_once('clases/internas/internasDeLaInterface/Evento.php');
?>