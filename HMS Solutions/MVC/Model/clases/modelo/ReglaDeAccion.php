<?php

    class ReglaDeAccion implements ICurl {

        public static $add = 'reglaDeAccion/add';
        public static $get = 'reglaDeAccion/get?id=';
        public static $getAll = 'reglaDeAccion/getAll';
        public static $delete = 'reglaDeAccion/delete?id=';
        public static $getHabitaciones = 'reglaDeAccion/getHabitaciones?id=';
        public static $getReglaDeAccionByMes = 'reglaDeAccion/getReglasByMes?mes=';
        public static $getLogs = 'reglaDeAccion/getLogs?id=';

        public static function get($id) {
            $curl = CurlControlador::Singleton();
            $resultado = $curl->recogerDatosGET(CurlControlador::$server . ReglaDeAccion::$get . $id);

            if(is_null($resultado)) {
                return NULL;
            }
            else{
                $asignacion = new ReglaDeAccion();
                $asignacion->addData($resultado);
                return $asignacion;
            }
        }

        public static function getByMesAno($mes, $ano) {
            $curl = CurlControlador::Singleton();
            $resultado = $curl->recogerDatosGET(CurlControlador::$server . ReglaDeAccion::$getReglaDeAccionByMes . $mes . '&ano=' . $ano);

            if(is_null($resultado)) {
                return NULL;
            }
            else{
                $reglas = [];
                foreach($resultado as $array) {
                    $regla = new ReglaDeAccion();
                    $regla->addData($array);
                    array_push($reglas, $regla);
                }
                return $reglas;
            }
        }

        private $id;
        private $nombreRegla;
        private $descripcion;
        private $tipoAccion; //enum
        private $fechaAlta;
        private $fechaBaja;
        private $usuario;
        private $habitaciones;
        private $calendario;

        public function delete() {
            $curl = CurlControlador::Singleton();
            $resultado = $curl->recogerDatosGET(CurlControlador::$server . ReglaDeAccion::$delete . $this->id);

            if(is_null($resultado)) {
                return NULL;
            }
            else{
                return $resultado;
            }
        }

        function __construct() {
            $this->fechaAlta = str_replace(' ', 'T', date('Y-m-d H:m:s.000O')); //añadimos la fecha de alta actual
            $this->habitaciones = [];
        }

        public function getId() {
            return $this->id;
        }

        public function getNombreRegla() {
            return $this->nombreRegla;
        }

        public function setNombreRegla($nombre) {
            $this->nombreRegla = $nombre;
        }

        public function getDescripcion() {
            return $this->descripcion;
        }

        public function setDescripcion($descripcion) {
            $this->descripcion = $descripcion;
        }

        public function getTipoAccion() {
            return $this->tipoAccion;
        }

        public function setTipoAccion($tipo) {
            $this->tipoAccion = $tipo;
        }

        public function getFechaAlta() {
            return $this->fechaAlta;
        }

        public function getFechaBaja() {
            return $this->fechaBaja;
        }

        public function setFechaBaja($fecha) {
            $this->fechaBaja = $fecha;
        }

        public function getHabitaciones() {
            return $this->habitaciones;
        }

        public function setHabitaciones($habitaciones) {
            $this->habitaciones = $habitaciones;
        }

        public function getUsuario() {
            return $this->usuario;
        }

        public function setUsuario($usuario) {
            $this->usuario = $usuario;
        }

        public function getCalendario() {
            return $this->calendario;
        }

        public function setCalendario($calendario) {
            $this->calendario = $calendario;
        }

        public function toString() : String {
            $cadena = 'ReglaDeAccion: id: ' . $this->getId() . ' nombreRegla: ' . $this->getNombreRegla() . ' fecha: ' . $this->getCalendario()->getDia() . '/' . $this->getCalendario()->getMes() . '/' . $this->getCalendario()->getAno() . ' hora: ' . $this->getCalendario()->getHora() . ':' . $this->getCalendario()->getMinuto() . ':' . $this->getCalendario()->getSegundo();
            $cadena = $cadena . ' Desc:' .  $this->descripcion;
            return $cadena;
        }

        public function guardar() {
            $curl = CurlControlador::Singleton();
            $resultado = $curl->recogerDatosPOST(CurlControlador::$server . ReglaDeAccion::$add, $this->getData());

            if(is_null($resultado)) {
                return NULL;
            }
            else{
                if(is_numeric($resultado)) {
                    if($resultado > 0) {
                        $this->id = $resultado;
                    }
                    return $resultado;
                }
                else {
                    return NULL;
                }
            }
        }

        public function getData() : Array
        {
            $var = get_object_vars($this);
            foreach($var as $value => &$val) {
                if (is_object($val) && method_exists($val,'getData')) {
                    $val = $val->getData();
                }
                elseif(is_array($val)) {
                    $array = [];
                    foreach($val as $localDate) {
                        if(is_object($localDate) && method_exists($localDate, 'getData')) {
                            array_push($array, $localDate->getData());
                        }
                        else {
                            array_push($array, $localDate);
                        }
                    }
                    $val = $array;
                }
            }
            return $var;
        }

        public function addData($data) //terminar
        {
            foreach($data as $key => $value) {
                switch($key) {
                    case 'usuario':
                        $this->usuario = new Usuario();
                        $this->usuario->addData($data->{$key});
                    break;
                    case 'calendario':
                        $this->calendario = new Calendario();
                        $this->calendario->addData($data->{$key});
                    break;
                    case 'habitaciones':
                        $this->habitaciones = [];
                        foreach($value as $array) {
                            $habitacion = new Habitacion();
                            $habitacion->addData($array);
                            array_push($this->habitaciones, $habitacion);
                        }
                    break;
                    default:
                        $this->{$key} = $value;
                }
            }
        } 
    }

?>