<?php
function fechaComparator($fecha1, $fecha2) : Int {
    //echo $fecha1;
    $hora1 = intval(substr($fecha1, 11, 2));
    $minuto1 = intval(substr($fecha1, 14, 2));
    $segundo1 = intval(substr($fecha1, 17, 2));
    $dia1 = intval(substr($fecha1, 8, 2));
    $mes1 = intval(substr($fecha1, 5, 2));
    $ano1 = intval(substr($fecha1, 0, 4));
    //echo 'fecha1: ' . $fecha1;

    $hora2 = intval(substr($fecha2, 11, 2));
    $minuto2 = intval(substr($fecha2, 14, 2));
    $segundo2 = intval(substr($fecha2, 17, 2));
    $dia2 = intval(substr($fecha2, 8, 2));
    $mes2 = intval(substr($fecha2, 5, 2));
    $ano2 = intval(substr($fecha2, 0, 4));

    if($ano1 < $ano2) return -1;
    elseif($mes1 < $mes2) return -1;
    elseif($dia1 < $dia2) return -1;
    elseif($hora1 < $hora2) return -1;
    elseif($minuto1 < $minuto2) return -1;
    elseif($segundo1 < $segundo2) return -1;
    else return 0;
}

function getFecha($ob) {
    $dd = NULL;
    switch(get_class($ob)) {
        case 'Asignacion':
        if(is_null($ob->getFechaEliminacion())) $dd = $ob->getFechaRealizacion();
        else $dd = $ob->getFechaEliminacion();
        break;
        case 'Movimiento':
            $dd = $ob->getFechaAccion();
        break;
        case 'LogPuerta':
            $dd = $ob->getFechaRealizacion();
        break;
    }
    return $dd;
}
?>