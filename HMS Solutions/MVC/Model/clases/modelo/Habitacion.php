<?php

    class Habitacion implements ICurl {
        public static $add = 'habitacion/add';
        public static $get = 'habitacion/get?id='; //GET
        public static $getAll = 'habitacion/getAll'; //GET
        public static $delete = 'habitacion/delete?id='; //GET
        public static $getLogPuerta = 'habitacion/getLogPuerta?id='; //GET
        public static $getTipoHabitacion = 'habitacion/getTipoHabitacion?id='; //GET
        public static $getAsignaciones = 'habitacion/getAsignaciones?id='; //GET
        public static $getMovimientos = 'habitacion/getMovimientos?id=';
        public static $getReglasDeAccion = 'habitacion/getReglasDeAccion?id=';
        public static $getByNumHabitacion = 'habitacion/getHabitacionByNumHabitacion?num=';
        public static $toggleDoor = 'habitacion/toggleDoor?num=';

        public static function getByNumHabitacion($num) {
            $curl = CurlControlador::Singleton();
            $resultado = $curl->recogerDatosGET(CurlControlador::$server . Habitacion::$getByNumHabitacion . $num);
            if(is_null($resultado)){
                return NULL;
            }
            else {
                $habitacion = new Habitacion();
                $habitacion->addData($resultado);
                return $habitacion;
            }
        }

        public static function toggleDoor($num) { //num es un string
            $curl = CurlControlador::Singleton();
            $resultado = $curl->recogerDatosGET(CurlControlador::$server . Habitacion::$toggleDoor . $num);
            if(is_null($resultado)){
                return FALSE;
            }
            else {
                return $resultado;
            }
        }

        public static function getAll() {
            $localCurl = CurlControlador::Singleton();
            $habitaciones = $localCurl->recogerDatosGET(CurlControlador::$server . Habitacion::$getAll);
            if(is_null($habitaciones)) {
                return NULL;
            }
            else {
                $habs = [];
                foreach($habitaciones as $array) {
                    $hab = new Habitacion();
                    $hab->addData($array);
                    array_push($habs, $hab);
                }
                return $habs;
            }
        }
        
        public static function getAllFromZona($zona) {
            $localCurl = CurlControlador::Singleton();
            $habitaciones = $localCurl->recogerDatosGET(CurlControlador::$server . Habitacion::$getAll);
            if(is_null($habitaciones)) {
                return NULL;
            }
            else {
                $habs = [];
                foreach($habitaciones as $array) {
                    $hab = new Habitacion();
                    $hab->addData($array);
                    if($hab->getZona()->getId() == $zona){
                        array_push($habs, $hab);
                    }
                }
                return $habs;
            }
        }
        
        public static function get($id) {
            $curl = CurlControlador::Singleton();
            $get = $curl->recogerDatosGET(CurlControlador::$server . Habitacion::$get . $id);
            if(is_null($get)) {
                return NULL;
            }
            else {
                $hab = new Habitacion();
                $hab->addData($get);
                return $hab;
            }
        }
    
        public function delete() {
            $curl = CurlControlador::Singleton();
            $delete = $curl->recogerDatosGET(CurlControlador::$server . Habitacion::$delete . $this->id);
            if(is_null($delete)) {
                return false;
            }
            else {
                return $delete;
            }
        }

        private $id;
        private $numHabitacion;
        private $estadoPuerta; //enum
        private $fechaAlta;
        private $fehcaBaja;
        private $zona;
        //private $asignaciones;
        private $tipoHabitacion; //objeto
        //private $movimientos; //array
        //private $logsPuerta; //array

        //logs en quanto a las puertas
        public function getLogPuerta() {
            $curl = CurlControlador::Singleton();
            $logs = $curl->recogerDatosGET(CurlControlador::$server . Habitacion::$getLogPuerta . $this->id);
        
            if(is_null($logs)) {
                return NULL;
            }
            else {
                $logsArray = [];
                foreach($logs as $array) {
                    $logPuerta = new LogPuerta();
                    $logPuerta->addData($array);
                    array_push($logsArray, $logPuerta);
                }
                return $logsArray;
            }
        }
        
        
        
        //los presos que estan y han sido asignados a esta celda
        public function getAsignaciones() {
            $curl = CurlControlador::Singleton();
            $asignaciones = $curl->recogerDatosGET(CurlControlador::$server . Habitacion::$getAsignaciones . $this->id);
        
            if(is_null($asignaciones)) {
                return NULL;
            }
            else {
                $asignacionesArray = [];
                foreach($asignaciones as $array) {
                    $asignacion = new Asignacion();
                    $asignacion->addData($array);
                    array_push($asignacionesArray, $asignacion);
                }
                return $asignacionesArray;
            }
        }

        //recupera los movimientos de los presos
        public function getMovimientos() {
            $curl = CurlControlador::Singleton();
            $reglas = $curl->recogerDatosGET(CurlControlador::$server . Habitacion::$getMovimientos . $this->id);
        
            if(is_null($reglas)) {
                return NULL;
            }
            else {
                $reglasArray = [];
                foreach($reglas as $array) {
                    $regla = new ReglaDeAccion();
                    $regla->addData($array);
                    array_push($reglasArray, $regla);
                }
                return $reglasArray;
            }
        }

        //recupera las reglas de accion en referencia a esta habitacion
        public function getReglasDeAccion() {
            $curl = CurlControlador::Singleton();
            $movimientos = $curl->recogerDatosGET(CurlControlador::$server . Habitacion::$getReglasDeAccion . $this->id);
        
            if(is_null($movimientos)) {
                return NULL;
            }
            else {
                $movimientosArray = [];
                foreach($movimientos as $array) {
                    $movimiento = new Movimiento();
                    $movimiento->addData($array);
                    array_push($movimientosArray, $asignacion);
                }
                return $movimientosArray;
            }
        }

        function __construct() {
            //$this->logsPuerta = [];
            //$this->movimientos = [];
            //$this->asignaciones = [];
            $this->fechaAlta = str_replace(' ', 'T', date('Y-m-d H:m:s.000O')); //añadimos la fecha de alta actual
        }

        public function getId() {
            return $this->id;
        }

        public function getNumHabitacion() {
            return $this->numHabitacion;
        }

        public function setNumHabitacion($num){
            $this->numHabitacion = $num;
        }

        public function getFechaAlta() {
            return $this->fechaAlta;
        }

        public function getFechaBaja(){
            return $this->fechaBaja;
        }

        public function setFechaBaja($fecha) {
            $this->fechaBaja = $fecha;
        }

        /*public function getAsignaciones() {
            return $this->asignaciones;
        }

        public function setAsignaciones($asignaciones) {
            $this->asignaciones = $asignaciones;
        }*/

        public function getTipoHabitacion() {
            return $this->tipoHabitacion;
        }

        public function setTipoHabitacion($tipo) {
            $this->tipoHabitacion = $tipo;
        }

        public function getEstadoPuerta() {
            return $this->estadoPuerta;
        }

        public function setEstadoPuerta($estado) {
            $this->estadoPuerta = $estado;
        }

        public function getZona() {
            return $this->zona;
        }

        public function setZona($zona) {
            $this->zona = $zona;
        }

        public function toString() : String {
            $cadena = 'Habitacion: id: ' . $this->id . ' numHabitacion: ' . $this->numHabitacion . ' estadoPuerta: ' . $this->estadoPuerta;
            
            /*foreach($this->asignaciones as $asignacion) {
                $cadena = $cadena . '<br />-> ' . $asignacion->toString();
            }*/
            
            //$cadena = $cadena . '<br />-> ' . $this->tipoHabitacion->toString();
            
            /*foreach($this->movimientos as $movimiento) {
                $cadena = $cadena . '<br />-> ' . $movimiento->toString();
            }*/

            /*foreach($this->logsPuerta as $logPuerta) {
                $cadena = $cadena . '<br />-> ' . $logPuerta->toString();
            }*/

            //$cadena = $cadena . '<br />' . $this->zona->toString();
            return $cadena;
        }

        public function guardar() {
            $curl = CurlControlador::Singleton();
            $resultado = $curl->recogerDatosPOST(CurlControlador::$server . Habitacion::$add, $this->getData());
            if(is_null($resultado)) {
                return NULL;
            }
            else{
                if($resultado > 0) {
                    $this->id = $resultado;
                    return TRUE;
                }
            }
            return FALSE;
        }

        //convierte el objeto a una array con el contenido de las variables a json
        public function getData() : Array
        {
            $var = get_object_vars($this);
            foreach($var as $value => &$val) {
                if (is_object($val) && method_exists($val,'getData')) {
                    $val = $val->getData();
                }
                elseif(is_array($val)) {
                    $array = [];
                    foreach($val as $localDate) {
                        if(is_object($localDate) && method_exists($localDate, 'getData')) {
                            array_push($array, $localDate->getData());
                        }
                        else {
                            array_push($array, $localDate);
                        }
                    }
                    $val = $array;
                }
            }
            return $var;
        }

        //añade los datos a este objeto
        public function addData($data)
        {
            foreach($data as $key => $value) {
                switch($key) {
                    case 'tipoHabitacion':
                        $this->tipoHabitacion = new TipoHabitacion();
                        $this->tipoHabitacion->addData($data->{$key});
                    break;
                    case 'zona':
                        $this->zona = new Zona();
                        $this->zona->addData($data->{$key});
                    break;
                    default:
                        $this->{$key} = $value;
                }
            }
        }
    }

?>