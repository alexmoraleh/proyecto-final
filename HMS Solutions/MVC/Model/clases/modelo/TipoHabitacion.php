<?php

    class TipoHabitacion implements ICurl {
        public static $add = 'tipoHabitacion/add';
        public static $get = 'tipoHabitacion/get?id=';
        public static $getAll = 'tipoHabitacion/getAll';
        public static $delete = 'tipoHabitacion/delete?id=';
        public static $getHabitaciones = 'tipoHabitacion/getHabitaciones?id=';

        public static function getAll() {
            $curl = CurlControlador::Singleton();
            $getAll = $curl->recogerDatosGET(CurlControlador::$server . TipoHabitacion::$getAll);
            if(is_null($getAll)) {
                return NULL;
            }
            else {
                $tipos = [];
                foreach($getAll as $array) {
                    $tipo = new TipoHabitacion();
                    $tipo->addData($array);
                    array_push($tipos, $tipo);
                }
                return $tipos;
            }
        }
    
        public static function get($id) {
            $curl = CurlControlador::Singleton();
            $get = $curl->recogerDatosGET(CurlControlador::$server . TipoHabitacion::$get . $id);
            if(is_null($get)) {
                return NULL;
            }
            else {
                $tipo = new TipoHabitacion();
                $tipo->addData($get);
                return $tipo;
            }
        }
    
        public function delete() {
            $curl = CurlControlador::Singleton();
            $delete = $curl->recogerDatosGET(CurlControlador::$server . TipoHabitacion::$delete . $this->id);
            if(is_null($delete)) {
                return false;
            }
            else {
                return $delete;
            }
        }

        private $id;
        private $nombreTipo;
        private $descripcionTipo;
        private $capacidad;

        public function getId() {
            return $this->id;
        }

        public function getNombreTipo() {
            return $this->nombreTipo;
        }

        public function setNombreTipo($nombre) {
            $this->nombreTipo = $nombre;
        }

        public function getDescripcionTipo() {
            return $this->descripcionTipo;
        }

        public function setDescripcionTipo($descripcion) {
            $this->descripcionTipo = $descripcion;
        }

        public function getCapacidad() {
            return $this->capacidad;
        }

        public function setCapacidad($capacidad) {
            $this->capacidad = $capacidad;
        }

        public function toString () : String {
            $cadena = 'TipoHabitacion: id: ' . $this->id . ' nombreTipo: ' . $this->nombreTipo . ' descripcionTipo: ' . $this->descripcionTipo . ' capacidad: ' . $this->capacidad;
            return $cadena;
        }

        public function guardar() {
            $curl = CurlControlador::Singleton();
            $resultado = $curl->recogerDatosPOST(CurlControlador::$server . TipoHabitacion::$add, $this->getData());

            if(is_null($resultado)) {
                return NULL;
            }
            else{
                if(is_numeric($resultado)) {
                    if($resultado > 0) {
                        $this->id = $resultado;
                    }
                    return $resultado;
                }
                else {
                    return NULL;
                }
            }
        }

        //convierte el objeto a una array con el contenido de las variables a json
        public function getData() : Array
        {
            $var = get_object_vars($this);
            foreach($var as $value => &$val) {
                if (is_object($val) && method_exists($val,'getData')) {
                    $val = $val->getData();
                }
                elseif(is_array($val)) {
                    $array = [];
                    foreach($val as $localDate) {
                        if(is_object($localDate) && method_exists($localDate, 'getData')) {
                            array_push($array, $localDate->getData());
                        }
                        else {
                            array_push($array, $localDate);
                        }
                    }
                    $val = $array;
                }
            }
            return $var;
        }

        //añade los datos a este objeto
        public function addData($data)
        {
            foreach($data as $key => $value) {
                switch($key) {
                    default:
                        $this->{$key} = $value;
                }
            }
        }

    }

?>