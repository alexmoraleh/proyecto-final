<div id="home_page">
    <div id="marc_home">
        <img src="./../Views/img/alcatraz.jpg" alt="name">
        <img src="./../Views/img/celdas.jpg" alt="name">
        <img src="./../Views/img/alcatrazpsd.jpg" alt="name">
        <img src="./../Views/img/roompicture.jpeg" alt="name">
            <div class="circle">
                <img src="./../Views/img/faviconwhite.png" alt="icon">
            </div>
    </div>

    <div id="home_content">
        <div id="header_home_content">
            <h2>Bienvenido.</h2>
        </div>

        <div id="status_home_window">
            <h2 style="text-align:center; font-size:50px; font-weight:bold; margin-top:50px;"> Eventos del mes: </h2>
            <div id="status_home_sheet">
                
            </div>
        </div>
        <script>
            let date = new Date();
            let mes1 = date.getMonth();
            let mes2 = date.getMonth();
            let mes3 = date.getMonth();
            let ano1 = date.getFullYear();
            let ano2 = date.getFullYear();
            let ano3 = date.getFullYear();
            $.ajax({
                url : "../../tratarPeticiones.php",
                data : {"mes1" : mes1 ,"ano1": ano1, "mes2" : mes2 ,"ano2": ano2, "mes3" : mes3 ,"ano3": ano3},
                type : 'POST',
                // el tipo de información que se espera de respuesta
                dataType : 'json',
                success : function(data) {
                    console.log(data);
                    let results = "";
                    data.forEach(function(object) {
                        results = results + "<span style='font-size: 30px; width:100%'> DIA:"+ object.dia + "/ HORA:" + (object.hora).split(":")[0] +"/ACCIÓN:"+ object.tipo + " HABITACION: "+ object.numHabitacion +"</span>";
                    });
                    $("#status_home_sheet").html(results);
                }
            });
        </script>
    </div>
 </div>