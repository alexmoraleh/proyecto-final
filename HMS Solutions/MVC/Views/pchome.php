<?php
  if(file_exists('./MVC/Controller/YouAreLogged.php')) {
    require_once('./MVC/Controller/YouAreLogged.php');
  }
  else {
    header('Location: ../../index.php');
  }

?>

<div class="body-container container">
    <div class="zonas-container" id="zonas"> 

    </div>
    <div class="logs-container" id="logs">
      <span class="titleLog"><b>Logs de puertas:</b></span>
      <div class="logs-puertas" id="logs-puertas">
      </div>
      <div class="logs-reclusos" id="logs-reclusos">
      <span class="titleLog"><b>Logs de movimientos:</b></span>
        <div class="logs-movimientos" id="logs-movimientos">
        </div>
        <span class="titleLog"><b>Logs de asignaciones:</b></span>
        <div class="logs-asignacion" id="logs-asignacion">
        </div>
      </div>
    </div>
</div>

<script>

  $.ajax(
  {
    type: "POST",
    url: "./tratarPeticiones.php",
    data: {"zonas": true},
    success: function(data) {
      //console.log(JSON.parse(data)[0]);
      eliminarNodes("zonas-container");
      let i = 0;
      let div = null;
      JSON.parse(data).forEach(element => {
        if(i == 0) {
          div = document.createElement('div');
          div.className += 'dosZonas';
        }
        div.appendChild(crearModulo(element.id, element.edificio, element.planta, element.nombre, element.descripcion, element.estado));
                
        i++;
        if(i == 2) {
          i = 0;
          apendar(div, 'zonas');
        }
      });  
      apendar(div, 'zonas');
    },
    error: function(){
      console.log("error");
    }
  });

  function getLogs(zonaId) {
    $.ajax(
    {
      type: "POST",
      url: "./tratarPeticiones.php",
      data: {"logs": 30, "zona-Id": zonaId},
      success: function(data) {
        let zonas = document.getElementById('zonas');
        zonas.style = 'width: 40%';
        let allLogs = document.getElementById('logs');
        allLogs.style = 'width: 58%';
                
        eliminarNodes("logs-puertas");
        eliminarNodes("logs-asignacion");
        eliminarNodes("logs-movimientos");


        JSON.parse(data).forEach(element => {
          let dd = crearLog(element);

          if(element.tipoLog == 'LOG') {
            apendar(dd, 'logs-puertas');
          }
          else {
            if(element.tipoLog == 'MOVIMIENTO') {
              apendar(dd, 'logs-movimientos');
            }
            else {
              apendar(dd, 'logs-asignacion');
            }
          }
        });

        let logs = document.getElementById('logs-puertas');
        if(logs.childElementCount == 0) {
          logs.innerHTML = '<span class="noLogClass">No se han encontrado logs.</span>';
        }

        let movimientos = document.getElementById('logs-movimientos');
        if(movimientos.childElementCount == 0) {
          movimientos.innerHTML = '<span class="noLogClass">No se han encontrado logs.</span>';
        }

        let asignaciones = document.getElementById('logs-asignacion');
        if(asignaciones.childElementCount == 0) {
          asignaciones.innerHTML = '<span class="noLogClass">No se han encontrado logs.</span>';
        }
      },
      error: function(){
        console.log("error");
      }
    });
  }

</script>