<div id="login_body">
    <div class="login">
    <div class="heading">
        <div id="logo_place" class="circle">
            <img src="./../Views/img/faviconwhite.png" alt="logo">
        </div>
        
        <h2>Sign in</h2>
        <form id="login_form" method="post" onsubmit="ajaxLogin()">

        <div class="input-group input-group-lg">
            <span class="input-group-addon"><i class="fa fa-user"></i></span>
            <input type="text" class="form-control" placeholder="Usuario" name="nombreUsuario">
            </div>

            <div class="input-group input-group-lg">
            <span class="input-group-addon"><i class="fa fa-lock" id="tonto"></i></span>
            <input type="password" id="pass_text" class="form-control" placeholder="Contraseña" name="password">
            </div>

            <button type="button" onclick="ajaxLogin()" class="float">Login</button>
            <div id="notify_bar"></div>
        </form>
            </div>
    </div>

</div>

<script>
function ajaxLogin(){
    var e = $("#login_form");
    // this.preventDefault(); // avoid to execute the actual submit of the form.
    var form = $(e);
    $.ajax({
           type: "POST",
           url:  "./../Controller/peticion_login.php",
           data: form.serialize(), // serializes the form's elements.
           success: function(data)
           {
               if(data == false){
                    // alert("dentro");
                    // alert(data);
                    let notfy = "<span style='color:red; font-size:40px;'>Incorrect password or user.</span>";
                    $("#notify_bar").children().remove();
                    $("#notify_bar").append(notfy); // show response from the php script.
                    $("#pass_text").val('');
               } else { 
                    window.location.href = './../Controller/controlador_movil.php?opt=1';   
               }
           },
           error: function(){
            //    alert("fallo");
           }
         });
        //  alert("xd");
}
// $("#login_form").submit(ajaxLogin(this));

</script>