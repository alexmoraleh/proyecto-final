<?php
// https://www.forevolve.com/en/articles/2016/08/12/how-to-add-jquery-intellisense-to-a-visual-studio-code-javascript-file/
    function gohome(){
        // if sesion iniciada
        $title = 'HomeBackend';
        $styles = '<link rel="stylesheet" href="./MVC/Views/css/navBar.css" type="text/css"/>
        <link rel="stylesheet" href="./MVC/Views/css/pchome.css" type="text/css"/>';
        require_once('./MVC/Views/Common/head.php');
        require_once('./MVC/Views/Common/header.php');
        require_once('./MVC/Views/pchome.php');
        echo "<script src='./MVC/Views/js/homeBackend.js'></script>";
        require_once('./MVC/Views/Common/footer.php');
        // else mandar al login
    }

    function goLogin() {
        $title = 'Login';
        if(file_exists('./MVC/Views/Common/head.php')) {
            $styles = '<link rel="stylesheet" href="./MVC/Views/css/login.css" type="text/css"/>';
            require_once('./MVC/Views/Common/head.php');
            require_once('./MVC/Views/login.php');
        }
        else {
            $styles = '<link rel="stylesheet" href="../Views/css/login.css" type="text/css"/>';
            require_once('../Views/Common/head.php');
            require_once('../Views/login.php');
        }
    }

    if(isset($_SESSION['login'])) { //isset($_SESSION['login'])
        if(isset($_GET['opt'])){
            // echo"<script>alert('hola');</script>";
            $opt = filter_var($_GET['opt'], FILTER_SANITIZE_NUMBER_INT);
            $styles = "";
            $title = "";
            switch(intval(trim($opt))){
                case 1:
                    $title = "Calendario";
                    $styles = '<link rel="stylesheet" href="./MVC/Views/css/calendario.css" type="text/css"/>';

                    require_once('./MVC/Views/Common/head.php');
                    require_once('./MVC/Views/Common/header.php');
                    require_once('./MVC/Views/calendario.php');
                    echo "<script src='./MVC/Views/js/calendario.js'></script>";
                    require_once('./MVC/Views/Common/footer.php');
                break;
                case 2:
                    $title = "Celdas";
                    $styles = '<link rel="stylesheet" href="./MVC/Views/css/celdas.css"/>';

                    require_once('./MVC/Views/Common/head.php');
                    require_once('./MVC/Views/Common/header.php');
                    require_once('./MVC/Views/celdas.php');
                    echo "<script src='./MVC/Views/js/celdas.js'></script>";
                    require_once('./MVC/Views/Common/footer.php');
                break;
                case 3:
                    $title = "Perfil";
                    $styles = '<link rel="stylesheet" href="./MVC/Views/css/perfil.css"/>
                    <link rel="stylesheet" href="./MVC/Views/css/navBar.css"/>';
                    require_once('./MVC/Views/Common/head.php');
                    require_once('./MVC/Views/Common/header.php');
                    require_once('./MVC/Views/perfil.php');
                    echo "<script src='./MVC/Views/js/perfil.js'></script>";
                    require_once('./MVC/Views/Common/footer.php');
                break;
                case 4: //logout
                    session_destroy();
                    header("Location: ./MVC/Views/Front End");
                    // goLogin(); //deveria ir al frontend
                break;
                default: 
                    gohome();
            }
        }
        else{
            gohome();
        }
    }
    else {
        //return to login
        goLogin();
    }
?>