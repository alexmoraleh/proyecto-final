<?php

    if(file_exists('./MVC/Controller/YouAreLogged.php')) {
        require_once('./MVC/Controller/YouAreLogged.php');
    }
    else {
        header('Location: ../YouAreLogged.php');
    }

    if(isset($_POST['id']) && isset($_POST['borrar'])) { //ok, falta probar-lo con el calendario
        $id = filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT);
        $borrar = filter_var($_POST['borrar'], FILTER_VALIDATE_BOOLEAN);

        //$id = 1;
        //$borrar = true;
        if($borrar == true) {
            $evento = ReglaDeAccion::get($id);
            if(!is_null($evento)) {
                print($evento->delete());
            }
        }
    }
?>