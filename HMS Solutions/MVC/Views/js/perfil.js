$(document).ready(recivir());


function recivir(){
    var urle = "";
    if (screen.width < 460){
        urle = "../../tratarPeticiones.php";
    } else {
        urle = "./tratarPeticiones.php";
    }
    $.ajax({
        url : urle,
        data : {"perfilg" : true},
        type : 'POST',
        // el tipo de información que se espera de respuesta
        dataType : 'json',
        success : function(data) {
            console.log(data);
            rellenar(data);
        },
        error : function(jqXHR, status, error) {
            $('#sect').prepend("<div class='alert alert-danger alert-dismissible fade show' role='alert'>Ha habido un error, no se ha podido acceder a tu información.<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>");
            console.log(jqXHR);
            console.log(status);
            console.log(error);
        }
    });
}
function rellenar(o){
    $("#nom").html(o.nombrec);
    $("#mail").html(o.mail);
    $("#naci").html(o.nacimiento);
    $("#nusr").html(o.usuario);
    $("#fec").html(o.alta.substring(0,10));
    var rol="";
    o.rol.forEach(e => {
        rol=rol+e.nombre+", ";
    });
    rol=rol.substring(0,rol.length-2);
    $("#rol").html(rol);
    $("#nombre").html(o.nombre);
    var aux=999999;
    o.rol.forEach(e => {
        if(aux>e.id){
            rol=e.nombre;
            aux=e.id;
        }
    });
    $("#prevs").html(rol);
    if (screen.width < 460){
        urle = "../../";
    } else {
        urle = "./";
    }
    $("#usr").attr("src", urle+"MVC/Views/img/"+o.img);
}