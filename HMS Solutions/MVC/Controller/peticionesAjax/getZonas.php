<?php

if(file_exists('./MVC/Controller/YouAreLogged.php')) {
    require_once('./MVC/Controller/YouAreLogged.php');
}
else {
    header('Location: ../YouAreLogged.php');
}

if(isset($_POST['zonas'])) {
    $zona = filter_var($_POST['zonas'], FILTER_VALIDATE_BOOLEAN);

    if($zona) {
        $allZonas = Zona::getAll();
        $allZonasToSend = [];
        foreach($allZonas as $zona) {
            array_push($allZonasToSend, $zona->getData());
        }

        print(json_encode($allZonasToSend));
    }
}
?>