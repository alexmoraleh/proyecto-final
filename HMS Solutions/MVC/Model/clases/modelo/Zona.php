<?php

    class Zona implements ICurl {
        public static $add = 'zona/add';
        public static $getAll = 'zona/getAll';
        public static $get = 'zona/get?id=';
        public static $delete = 'zona/delete?id=';
        public static $getHabitaciones = 'zona/getHabitaciones?id=';

        public static function getAll() {
            $curl = CurlControlador::Singleton();
            $zonasResultado = $curl->recogerDatosGET(CurlControlador::$server . Zona::$getAll);
            if(is_null($zonasResultado)) {
                return NULL;
            }
            else {
                $zonas = [];
                foreach($zonasResultado as $array) {
                    $zona = new Zona();
                    $zona->addData($array);
                    array_push($zonas, $zona);
                }
                return $zonas;
            }
        }

        public static function get($id) {
            $curl = CurlControlador::Singleton();
            $zonaResultado = $curl->recogerDatosGET(CurlControlador::$server . Zona::$get . $id);
            if(is_null($zonaResultado)) {
                return NULL;
            }
            else {
                $zona = new Zona();
                $zona->addData($zonaResultado);
                return $zona;
            }
        }

        public function delete() {
            $curl = CurlControlador::Singleton();
            $zonaResultado = $curl->recogerDatosGET(CurlControlador::$server . Zona::$delete . $this->id);
            if(is_null($zonaResultado)) {
                return false;
            }
            else {
                return $zonaResultado;
            }
        }

        public static function getHabitaciones($id) {
            $curl = CurlControlador::Singleton();
            $habitacionesResultado = $curl->recogerDatosGET(CurlControlador::$server . Zona::$getHabitaciones . $id);
            if(is_null($habitacionesResultado)) {
                return NULL;
            }
            else {
                $habitaciones = [];
                foreach($habitacionesResultado as $array) {
                    $habitacion = new Habitacion();
                    $habitacion->addData($array);
                    array_push($habitaciones, $habitacion);
                }
                return $habitaciones;
            }
        }

        private $id;
        private $nombre;
        private $descripcion;
        private $edificio;
        private $planta;
        private $estado;
        private $fechaAlta;
        private $fechaBaja;
        
        function __construct(){
            $this->fechaAlta = str_replace(' ', 'T', date('Y-m-d H:m:s.000O')); //añadimos la fecha de alta actual
        }

        public function getId() {
            return $this->id;
        }

        public function getNombre() {
            return $this->nombre;
        }

        public function setNombre($nombre) {
            $this->nombre = $nombre;
        }

        public function getDescripcion() {
            return $this->descripcion;
        }

        public function setDescripcion($descripcion) {
            $this->descripcion = $descripcion;
        }

        public function getEdificio() {
            return $this->edificio;
        }

        public function setEdificio($ed) {
            $this->edificio = $ed;
        }

        public function getPlanta() {
            return $this->planta;
        }

        public function setPlanta($planta) {
            $this->planta = $planta;
        }

        public function getEstado() {
            return $this->estado;
        }

        public function setEstado($estado) {
            $this->estado = $estado;
        }

        public function getFechaAlta() {
            return $this->fechaAlta;
        }

        public function getFechaBaja() {
            return $this->fechaBaja;
        }

        public function setFechaBaja($fechaBaja) {
            $this->fechaBaja = $fechaBaja;
        }

        public function toString() : String {
            $cadena = 'Zona: id:' . $this->id;
            return $cadena;
        }

        public function guardar() {
            $curl = CurlControlador::Singleton();
            $resultado = $curl->recogerDatosPOST(CurlControlador::$server . Zona::$add, $this->getData());

            if(!is_null($resultado)) {
                if(is_numeric($resultado)) {
                    if($resultado > 0) {
                        $this->id = $resultado;
                        return true;
                    }
                }
            }
            return false;
        }

        //convierte el objeto a una array con el contenido de las variables a json
        public function getData() : Array
        {
            $var = get_object_vars($this);
            foreach($var as $value => &$val) {
                if (is_object($val) && method_exists($val,'getData')) {
                    $val = $val->getData();
                }
                elseif(is_array($val)) {
                    $array = [];
                    foreach($val as $localDate) {
                        if(is_object($localDate) && method_exists($localDate, 'getData')) {
                            array_push($array, $localDate->getData());
                        }
                        else {
                            array_push($array, $localDate);
                        }
                    }
                    $val = $array;
                }
            }
            return $var;
        }

        //añade los datos a este objeto
        public function addData($data)
        {
            foreach($data as $key => $value) {
                switch($key) {
                    default:
                        $this->{$key} = $value;
                }
            }
        }
    }

?>