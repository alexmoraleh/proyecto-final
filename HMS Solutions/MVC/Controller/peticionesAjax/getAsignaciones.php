<?php
    session_start();
    require_once('../../Model/BasicRequirements.php');

    if(isset($_POST['door']) and isset($_SESSION['login'])) {
        
        $habi = filter_var($_POST['door'], FILTER_SANITIZE_NUMBER_INT);
        $habiVerdad = null;
        foreach(Habitacion::getAll() as $hab){
            if($hab->getNumHabitacion() == $habi){
                $habiVerdad = $hab;
            }
        }
        $asignaciones = [];
        foreach($habiVerdad->getAsignaciones() as $asignacion) {
            if(is_null($asignacion->getFechaEliminacion())) {
                array_push($asignaciones, $asignacion->getData());
            }
        }
        print_r(json_encode($asignaciones));
    }
?>