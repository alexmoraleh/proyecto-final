<?php

class Usuario implements ICurl {

    public static $add = 'usuario/add';
    public static $getAll = 'usuario/getAll';
    public static $get = 'usuario/get?id=';
    public static $getPrivilegios = 'usuario/getPrivilegios?id=';
    public static $login = 'usuario/login'; //POST PETICIONLOGIN
    public static $delete = 'usuario/delete?id=';
    public static $getLogsPuertas = 'usuario/getLogsPuertas?id=';
    public static $getReglasAccion = 'usuario/getReglasAccion?id=';
    public static $exist = 'usuario/exist?id=';
    

    public static function getAll() {
        $curl = CurlControlador::Singleton();
        $usuariosRespuesta = $curl->recogerDatosGET(CurlControlador::$server . Usuario::$getAll);
        if(is_null($usuariosRespuesta)) {
            return NULL;
        }
        else {
            $usuarios = [];
            foreach($usuariosRespuesta as $array) {
                $usuario = new Usuario();
                $usuario->addData($array);
                array_push($usuarios, $usuario);
            }
            return $usuarios;
        }
    }

    public static function get($id) {
        $curl = CurlControlador::Singleton();
        $usuarioResultado = $curl->recogerDatosGET(CurlControlador::$server . Usuario::$get . $id);
        
        if(is_null($usuarioResultado)) {
            return NULL;
        }
        else {
            $usuario = new Usuario();
            $usuario->addData($usuarioResultado);
            return $usuario;
        }
    }

    public static function exist($id) {
        $curl = CurlControlador::Singleton();
        $usuarioResultado = $curl->recogerDatosGET(CurlControlador::$server . Usuario::$exist . $id);
        if(is_null($usuarioResultado)) {
            return NULL;
        }
        else {
            return $usuarioResultado;
        }
    }

    public function delete() {
        $curl = CurlControlador::Singleton();
        $usuarioResultado = $curl->recogerDatosGET(CurlControlador::$server . Usuario::$delete . $this->id);
        if(is_null($usuarioResultado)) {
            return false;
        }
        else {
            return $usuarioResultado;
        }
    }

    private $id = null;
    private $nombreUsuario;
    private $password;
    private $nombre;
    private $apellido;
    private $segundoApellido;
    private $mail;
    private $imagenUsuario = "usuario.png"; //imagen de usuario por defecto
    private $fechaAlta;
    private $fechaBaja;
    private $privilegiosUsuario;
    private $token = null;
    private $fechaNacimiento;

    function __construct() {
        $this->fechaAlta = str_replace(' ', 'T', date('Y-m-d H:m:s.000O')); //añadimos la fecha de alta actual
        $this->privilegiosUsuario = [];
    }

    public function getId() {
        return $this->id;
    }

    public function getNombreUsuario() {
        return $this->nombreUsuario;
    }

    public function setNombreUsuario($nombre) {
        $this->nombreUsuario = $nombre;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function getApellido() {
        return $this->apellido;
    }

    public function setApellido($apellido) {
        $this->apellido = $apellido;
    }

    public function getSegundoApellido() {
        return $this->segundoApellido;
    }

    public function setSegundoApellido($segundoApellido) {
        $this->segundoApellido = $segundoApellido;
    }

    public function getMail() {
        return $this->mail;
    }

    public function setMail($mail) {
        $this->mail = $mail;
    }

    public function setPassword($password) {
        $this->password = hash("sha256", $password);
    }

    public function getImagenUsuario() {
        return $this->imagenUsuario;
    }

    public function setImagenUsuario($imagen) {
        $this->imagenUsuario = $imagen;
    }

    public function getFechaAlta() {
        return $this->fechaAlta;
    }

    public function getFechaBaja() {
        return $this->fechaBaja;;
    }

    public function setFechaBaja($fecha) {
        $this->fechaBaja = $fecha;
    }

    public function getPrivilegiosUsuario() {
        return $this->privilegiosUsuario;
    }

    public function setPrivilegiosUsuario($privilegios) {
        $this->privilegiosUsuario = $privilegios;
    }

    public function getToken() {
        return $this->token;
    }

    public function setToken($token) {
        $this->token = $token;
    }

    public function getFechaNacimiento() {
        return $this->fechaNacimiento;
    }

    public function setFechaNacimiento($fecha) {
        $ano = substr($fecha, 0, 4);
        $mes = substr($fecha, 5, 2);
        $dia = substr($fecha, 8);

        if(checkdate(intval($mes), intval($dia), intval($ano))) {
            $f = date_format(date_create($fecha), 'Y-m-d H:m:s.000O');
            $this->fechaNacimiento = str_replace(' ', 'T', $f);
            return true;
        }
        return false;
    }

    public function toString() : String {
        $cadena = 'Usuario: id: ' . $this->getId() . ' nombreUsuario: ' . $this->getNombreUsuario() . ' mail: ' . $this->getMail() . ' fechaAlta: ' . $this->getFechaAlta();
        
        foreach($this->getPrivilegiosUsuario() as $privilegio) {
            if(method_exists($privilegio, 'toString')) $cadena = $cadena . '<br />' . $privilegio->toString();
        }

        $cadena = $cadena . '<br />Token: ' . $this->getToken();
        
        return $cadena;
    }

    public function verifyPassword($password) {
        if($this->password == $password) {
            return true;
        }
        return false;
    }

    public function guardar() {
        $curl = CurlControlador::Singleton();
        $resultado = $curl->recogerDatosPOST(CurlControlador::$server . Usuario::$add, $this->getData());

        if(is_null($resultado)) {
            return NULL;
        }
        else{
            if(is_numeric($resultado)) {
                if($resultado > 0) {
                    $this->id = $resultado;
                }
                return $resultado;
            }
            else {
                return NULL;
            }
        }
    }

    function getPrivilegios($usuario) {

        $curl = CurlControlador::Singleton();
        $resultado = $curl->recogerDatosGET(CurlControlador::$server . Usuario::$getPrivilegios . $usuario->getId());
        $privilegios = [];

        foreach($resultado as $array) {
            $privilegio = new Privilegio();
            $privilegio->addData($array);
            array_push($privilegios, $privilegio);
        }

        $this->privilegios = $privilegios;
    }

    public function getData() : Array
    {
        $var = get_object_vars($this);
        foreach($var as $value => &$val) {
            if (is_object($val) && method_exists($val,'getData')) {
                $val = $val->getData();
            }
            elseif(is_array($val)) {
                $array = [];
                foreach($val as $localDate) {
                    if(is_object($localDate) && method_exists($localDate, 'getData')) {
                        array_push($array, $localDate->getData());
                    }
                    else {
                        array_push($array, $localDate);
                    }
                }
                $val = $array;
            }
        }
        return $var;
    }

    public function addData($data)
    {
        foreach($data as $key => $value) {
            switch($key) {
                case 'privilegioUsuario':
                    $this->privilegiosUsuario = [];

                    foreach($value as $array) {
                        $privilegio = new Privilegio();
                        $privilegio->addData($array);
                        array_push($this->privilegiosUsuario, $privilegio); 
                    }
                break;
                default:
                    $this->{$key} = $value;
            }
        }
    }
}

?>