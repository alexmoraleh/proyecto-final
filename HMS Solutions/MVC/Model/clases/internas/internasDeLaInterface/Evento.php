<?php
    class Evento {
        private $id;
        private $descripcion;
        private $tipo;
        private $nombreUsuario;
        private $numHabitacion;
        private $dia;
        private $mes;
        private $ano;
        private $hora;

        function __construct() {
        }

        public function addData($id, $descripcion, $tipo, $nombreUsuario, $numHabitacion, $dia, $mes, $ano, $hora) {
            $this->id = $id;
            $this->descripcion = $descripcion;
            $this->tipo = $tipo;
            $this->nombreUsuario = $nombreUsuario;
            $this->numHabitacion = $numHabitacion;
            $this->mes = $mes;
            $this->ano = $ano;
            $this->dia = $dia;
            $this->hora = $hora;
        }

        public function getData() : Array
        {
            return get_object_vars($this);
        }
    }

?>