<?php

    class MotivoAccionLogPuerta implements ICurl {
        public static $add = 'motivoAccionLogPuerta/add';
        public static $get = 'motivoAccionLogPuerta/get?id='; //GET
        public static $getAll = 'motivoAccionLogPuerta/getAll'; //GET
        public static $delete = 'motivoAccionLogPuerta/delete?id='; //GET
        public static $getLogsPuertas = 'motivoAccionLogPuerta/getLogsPuertas?id='; //GET
        public static $getMotivoByNombreMotivo = 'motivo/getMotivoByNombreMotivo';//POST

        public static function get($id) {
            $curl = CurlControlador::Singleton();
            $resultado = $curl->recogerDatosGET(CurlControlador::$server . MotivoAccionLogPuerta::$get . $id);

            if(is_null($resultado)) {
                return NULL;
            }
            else{
                $asignacion = new Asignacion();
                $asignacion->addData($resultado);
                return $asignacion;
            }
        }

        public static function getAll() {
            $curl = CurlControlador::Singleton();
            $resultado = $curl->recogerDatosGET(CurlControlador::$server . MotivoAccionLogPuerta::$getAll);

            if(is_null($resultado)) {
                return NULL;
            }
            else{
                $motivos = [];
                foreach($resultado as $array) {
                    $motivo = new MotivoAccionLogPuerta();
                    $motivo->addData($array);
                    array_push($motivos, $motivo);
                }
               //print_r($asignaciones);
                return $motivos;
            }
        }

        public function getMotivoByNombreMotivo($nombre) {
            $curl = CurlControlador::Singleton();
            $resultado = $curl->recogerDatosPOST(CurlControlador::$server . MotivoAccionLogPuerta::$getMotivoByNombreMotivo, strval($nombre));
    
            if(is_null($resultado)) {
                return NULL;
            }
            else{
                $motivo = new MotivoAccionLogPuerta();
                $motivo->addData($resultado);
                return $motivo;
            }
        }

        private $id;
        private $nombreMotivo;
        private $descripcion;

        function __construct() {

        }

        public function getId() {
            return $this->id;
        }

        public function getNombreMotivo() {
            return $this->nombreMotivo;
        }

        public function setNombreMotivo($nombre) {
            $this->nombreMotivo = $nombre;
        }

        public function getDescripcion() {
            return $this->descripcion;
        }

        public function setDescripcion($descripcion) {
            $this->descripcion = $descripcion;
        }

        public function toString() : String {
            return 'MotivoAccionLogPuerta: id: ' . $this->getId() . ' nombreMotivo: ' . $this->getNombreMotivo() . ' descripcion: ' . $this->getDescripcion();
        }

        public function guardar() {
            $curl = CurlControlador::Singleton();
            $resultado = $curl->recogerDatosPOST(CurlControlador::$server . MotivoAccionLogPuerta::$add, $this->getData());
    
            if(!is_null($resultado)) {
                if($resultado > 0) {
                    $this->id = $resultado;
                    return true;
                }
            }
            return false;
        }

        public function getData() : Array
        {
            $var = get_object_vars($this);
            foreach($var as $value => &$val) {
                if (is_object($val) && method_exists($val,'getData')) {
                    $val = $val->getData();
                }
                elseif(is_array($val)) {
                    $array = [];
                    foreach($val as $localDate) {
                        if(is_object($localDate) && method_exists($localDate, 'getData')) {
                            array_push($array, $localDate->getData());
                        }
                        else {
                            array_push($array, $localDate);
                        }
                    }
                    $val = $array;
                }
            }
            return $var;
        }

        public function addData($data)
        {
            foreach($data as $key => $value) {
                switch($key) {
                    default:
                        $this->{$key} = $value;
                }
            }
        }
    }

?>