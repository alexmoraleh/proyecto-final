<?php

    class Asignacion implements ICurl {
        public static $add = 'asignacion/add';
        public static $get = 'asignacion/get?id='; //GET
        public static $getAll = 'asignacion/getAll'; //GET
        public static $delete = 'asignacion/delete?id='; //GET
        public static $getRecluso = 'asignacion/getRecluso?id='; //GET
        public static $getHabitacion = 'asignacion/getHabitacion?id='; //GET
        
        public static function get($id) {
            $curl = CurlControlador::Singleton();
            $resultado = $curl->recogerDatosGET(CurlControlador::$server . Asignacion::$get . $id);

            if(is_null($resultado)) {
                return NULL;
            }
            else{
                $asignacion = new Asignacion();
                $asignacion->addData($resultado);
                return $asignacion;
            }
        }

        public static function getAll() {
            $curl = CurlControlador::Singleton();
            $resultado = $curl->recogerDatosGET(CurlControlador::$server . Asignacion::$getAll);

            if(is_null($resultado)) {
                return NULL;
            }
            else{
                $asignaciones = [];
                foreach($resultado as $array) {
                    $asignacion = new Asignacion();
                    $asignacion->addData($array);
                    array_push($asignaciones, $asignacion);
                }
               //print_r($asignaciones);
                return $asignaciones;
            }
        }

        private $id;
        private $fechaRealizacion;
        private $fechaEliminacion;
        private $recluso; //objeto
        private $habitacion;

        public function delete() {
            $curl = CurlControlador::Singleton();
            $resultado = $curl->recogerDatosGET(CurlControlador::$server . Asignacion::$delete . $this->id);

            if(is_null($resultado)) {
                return NULL;
            }
            else{
                return $resultado;
            }
        }
        
        function __construct() {
            $this->fechaRealizacion = str_replace(' ', 'T', date('Y-m-d H:m:s.000O')); //añadimos la fecha de alta actual
        }

        public function getId() {
            return $this->id;
        }

        public function getFechaRealizacion() {
            return $this->fechaRealizacion;
        }

        public function setFechaRealizacion($fecha) {
            $this->fechaRealizacion = $fecha;
        }

        public function getFechaEliminacion() {
            return $this->fechaEliminacion;
        }

        public function setFechaEliminacion($fecha) {
            $this->fechaEliminacion = $fecha;
        }

        public function getRecluso(){
            return $this->recluso;
        }

        public function setRecluso($recluso) {
            $this->recluso = $recluso;
        }

        public function getHabitacion() {
            return $this->habitacion;
        }

        public function setHabitacion($habitacion) {
            $this->habitacion = $habitacion;
        }

        public function toString() : String {
            $cadena = 'Asignacion: id: ' . $this->id . ' fechaRealizacion: ' . $this->fechaRealizacion . ' fechaEliminacion: ' . $this->fechaEliminacion;
            $cadena = $cadena . '<br />-> ' . $this->recluso->toString(); 
            return $cadena;
        }

        public function guardar() {
            $curl = CurlControlador::Singleton();
            $resultado = $curl->recogerDatosPOST(CurlControlador::$server . Asignacion::$add, $this->getData());

            if(is_null($resultado)) {
                return NULL;
            }
            else{
                if(is_numeric($resultado)) {
                    if($resultado > 0) {
                        $this->id = $resultado;
                    }
                    return $resultado;
                }
                else {
                    return NULL;
                }
            }
        }

        //convierte el objeto a una array con el contenido de las variables a json
        public function getData() : Array
        {
            $var = get_object_vars($this);
            foreach($var as $value => &$val) {
                if (is_object($val) && method_exists($val,'getData')) {
                    $val = $val->getData();
                }
                elseif(is_array($val)) {
                    $array = [];
                    foreach($val as $localDate) {
                        if(is_object($localDate) && method_exists($localDate, 'getData')) {
                            array_push($array, $localDate->getData());
                        }
                        else {
                            array_push($array, $localDate);
                        }
                    }
                    $val = $array;
                }
            }

            return $var;
        }

        //añade los datos a este objeto
        public function addData($data)
        {
            foreach($data as $key => $value) {
                switch($key) {
                    case 'recluso':
                        $this->recluso = new Recluso();
                        $this->recluso->addData($value);
                    break;
                    case 'habitacion':
                        $this->habitacion = new Habitacion();
                        $this->habitacion->addData($value);
                    break;
                    default:
                        $this->{$key} = $value;
                }
            }
        }

        /*public function toLog() {
            $log = new Log();

        }*/
    }

?>