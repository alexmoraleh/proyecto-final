 
        <div id="menu" >
            <div id="back" onclick="goBack()" class="module">
                <i class="fas fa-arrow-left"></i>
            </div>
            
            <div id="home" class="module" onClick="openModule(this)">
                <i class="fas fa-home"></i>
            </div>
            
            <div id="cells" class="module" onClick="openModule(this)">
                <i class="fas fa-unlock-alt"></i>
            </div>

            <div id="calendar" class="module" onClick="openModule(this)">
                <i class="fas fa-calendar-alt"></i>
            </div>

            <div id="Profile" class="module" onClick="openModule(this)">
                <i class="fas fa-address-card"></i>
            </div>

            <div id="logOut" class="module" onClick="openModule(this)">
                <i class="fas fa-sign-out-alt"></i>
            </div>
            
            <div id="hamburgesa" onclick="openMenu()" class="module">
                <i class="fas fa-bars"></i>
            </div>
 
        </div>
        
        <div id="cellMenu">
            <div id="openCell" onClick="confirmAction(this)">
                <i class="fas fa-lock-open"></i>
            </div>
            
            <div id="closeCell" onClick="confirmAction(this)" >
                <i class="fas fa-lock"></i>
            </div>
            
            
            
        </div>
            
            
            
        
    </body>
</html>