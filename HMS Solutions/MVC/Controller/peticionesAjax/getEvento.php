<?php

    if(file_exists('./MVC/Controller/YouAreLogged.php')) {
        require_once('./MVC/Controller/YouAreLogged.php');
    }
    else {
        header('Location: ../YouAreLogged.php');
    }

    if(isset($_POST['mes1']) && isset($_POST['ano1']) && isset($_POST['mes2']) && isset($_POST['ano2']) && isset($_POST['mes3']) && isset($_POST['ano3'])) {
        $arrayAll = [];

        //primer mes, recogemos los datos
        $mes = filter_var(intval($_POST['mes1']), FILTER_SANITIZE_NUMBER_INT) + 1;
        $ano = filter_var(intval($_POST['ano1']), FILTER_SANITIZE_NUMBER_INT);
        /*$mes = 4;
        $ano = 2019;*/
        $datos = ReglaDeAccion::getByMesAno($mes, $ano);

        foreach($datos as $accion) {
            $evento = new Evento();
            $calendario = $accion->getCalendario();
            $hora = $calendario->getHora() . ':' . $calendario->getMinuto() . ':' . $calendario->getSegundo();
            $habitaciones = $accion->getHabitaciones();
            $numHabitacion = [];
            foreach($habitaciones as $hab) {
                array_push($numHabitacion, $hab->getNumHabitacion());
            }
            $nombre=$accion->getUsuario()->getApellido() . ", " . $accion->getUsuario()->getNombre();
            $evento->addData($accion->getId(), $accion->getDescripcion(), $accion->getTipoAccion(),  $nombre, $numHabitacion, $calendario->getDia(), $calendario->getMes(), $calendario->getAno(), $hora);
            array_push($arrayAll, $evento->getData());
        }

        //print_r(json_encode($arrayAll));
        //echo '<br />';

        //segundo mes, recogemos los datos
        $mes = filter_var(intval($_POST['mes2']), FILTER_SANITIZE_NUMBER_INT) + 1;
        $ano = filter_var(intval($_POST['ano2']), FILTER_SANITIZE_NUMBER_INT);
        /*$mes = 5;
        $ano = 2019;*/
        $datos = ReglaDeAccion::getByMesAno($mes, $ano);

        foreach($datos as $accion) {
            $evento = new Evento();
            $calendario = $accion->getCalendario();
            $hora = $calendario->getHora() . ':' . $calendario->getMinuto() . ':' . $calendario->getSegundo();
            $habitaciones = $accion->getHabitaciones();
            $numHabitacion = [];
            foreach($habitaciones as $hab) {
                array_push($numHabitacion, $hab->getNumHabitacion());
            }
            $nombre=$accion->getUsuario()->getApellido() . ", " . $accion->getUsuario()->getNombre();
            $evento->addData($accion->getId(), $accion->getDescripcion(), $accion->getTipoAccion(),  $nombre, $numHabitacion, $calendario->getDia(), $calendario->getMes(), $calendario->getAno(), $hora);
            array_push($arrayAll, $evento->getData());
        }
        //print_r(json_encode($arrayAll));
        //echo '<br />';

        //tercer mes, recogemos los datos
        $mes = filter_var(intval($_POST['mes3']), FILTER_SANITIZE_NUMBER_INT) + 1;
        $ano = filter_var(intval($_POST['ano3']), FILTER_SANITIZE_NUMBER_INT);
        /*$mes = 6;
        $ano = 2019;*/
        $datos = ReglaDeAccion::getByMesAno($mes, $ano);

        foreach($datos as $accion) {
            $evento = new Evento();
            $calendario = $accion->getCalendario();
            $hora = $calendario->getHora() . ':' . $calendario->getMinuto() . ':' . $calendario->getSegundo();
            $habitaciones = $accion->getHabitaciones();
            $numHabitacion = [];
            foreach($habitaciones as $hab) {
                array_push($numHabitacion, $hab->getNumHabitacion());
            }
            $nombre=$accion->getUsuario()->getApellido() . ", " . $accion->getUsuario()->getNombre();
            $evento->addData($accion->getId(), $accion->getDescripcion(), $accion->getTipoAccion(),  $nombre, $numHabitacion, $calendario->getDia(), $calendario->getMes(), $calendario->getAno(), $hora);
            array_push($arrayAll, $evento->getData());
        }
        print(json_encode($arrayAll));
    }
?>