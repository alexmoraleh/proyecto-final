<?php

    if(file_exists('./MVC/Controller/YouAreLogged.php')) {
        require_once('./MVC/Controller/YouAreLogged.php');
    }
    else {
        header('Location: ../YouAreLogged.php');
    }

    if(isset($_POST['zonaId'])) {
        $zona = filter_var($_POST["zonaId"], FILTER_SANITIZE_NUMBER_INT);

        $habitaciones = [];
        foreach(Zona::getHabitaciones($zona) as $habitacion) {
            $asignaciones = [];
            foreach($habitacion->getAsignaciones() as $asignacion) {
                if(is_null($asignacion->getFechaEliminacion())) {
                    array_push($asignaciones, $asignacion->getData());
                }
            }

            $hab = $habitacion->getData();
            $hab['asignaciones'] = $asignaciones;
            array_push($habitaciones, $hab);
        }
        print_r(json_encode($habitaciones));
    }
?>