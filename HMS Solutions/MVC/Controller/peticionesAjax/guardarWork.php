<?php
    if(isset($_POST['nombre']) && isset($_POST['email']) && isset($_POST['tel']) && isset($_POST['dat'])){
        
        $nombre = filter_var($_POST['nombre'], FILTER_SANITIZE_STRING);
        $tel = filter_var($_POST['tel'], FILTER_SANITIZE_STRING);
        $dat = filter_var($_POST['dat'], FILTER_SANITIZE_STRING);
        $email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);

        if(len($nombre) > 0 && len($email) > 0 && len($dat) > 0 && len($tel) > 0) {
            $work = new Work();
            $work->nombre = filter_var($_POST['nombre'], FILTER_SANITIZE_STRING);
            $work->email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
            $work->tel = filter_var($_POST['tel'], FILTER_SANITIZE_STRING);
            $work->dat = filter_var($_POST['dat'], FILTER_SANITIZE_STRING);
            $resultado = $work->guardar();
            if(is_null($resultado)) {
                echo false;
            }
            elseif($resultado > 0) {
                echo true;
            }
        }
        echo false;
    }
?>